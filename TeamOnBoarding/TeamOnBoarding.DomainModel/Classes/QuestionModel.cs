﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamOnBoarding.DomainModel.Classes
{
    public class QuestionModel
    {
        public QuestionModel()
        {
            Options = new List<Option>();
        }
        public int QuestionId { get; set; }
        public int CourseId { get; set; }
        public string QuestionTitle { get; set; }
        public string QuestionType { get; set; }
        public int QuestionTime { get; set; }
        public List<Option> Options { get; set; }
    }

    public class Option
    {
        public string OptionTitle { get; set; }
        public int OptionId { get; set; }
    }
}
