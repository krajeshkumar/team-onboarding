﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamOnBoarding.DomainModel.DTO
{
	public class QuestionResponseDto
	{
		public int QuestionId { get; set; }
		public string Response { get; set; }
		public string QuestionType { get; set; }
	}
}
