using System.Collections.Generic;
using TeamOnBoarding.DomainModel.Common;

namespace TeamOnBoarding.DomainModel.Entities
{
	public class Question : GlobalEntity
	{
		public Question()
		{
			this.Options = new HashSet<Option>();
			this.UserAssessments = new HashSet<UserAssessment>();
		}

		public string QuestionDescription { get; set; }

		public int Time { get; set; }

		public int CourseId { get; set; }

		public string QuestionType { get; set; }

		public virtual Course Course { get; set; }

		public virtual ICollection<Option> Options { get; set; }

		public virtual ICollection<UserAssessment> UserAssessments { get; set; }
	}
}