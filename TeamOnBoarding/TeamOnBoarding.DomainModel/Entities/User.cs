using System;
using System.Collections.Generic;
using TeamOnBoarding.DomainModel.Common;

namespace TeamOnBoarding.DomainModel.Entities
{
	public class User : GlobalEntity
	{
		public User()
		{
			this.UserAssessments = new HashSet<UserAssessment>();
			this.UserAssessmentStatus = new HashSet<UserAssessmentStatus>();
		}

		public string Username { get; set; }

		public string FirstName { get; set; }

		public string LastName { get; set; }

		public string Password { get; set; }

		public string EmailAddress { get; set; }

		public bool IsActive { get; set; }

		public string Role { get; set; }

		public string Gender { get; set; }

		public int? TrackId { get; set; }

		public DateTime? CreatedDate { get; set; }

		public DateTime? ModifiedDate { get; set; }

		public virtual Track Track { get; set; }

		public virtual ICollection<UserAssessment> UserAssessments { get; set; }

		public virtual ICollection<UserAssessmentStatus> UserAssessmentStatus { get; set; }
	}
}