﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeamOnBoarding.DomainModel.Common;

namespace TeamOnBoarding.DomainModel.Entities
{
	public class UserAssessmentStatus : GlobalEntity
	{
		[NotMapped]
		public int Id { get; set; }

		public int UserId { get; set; }

		public int CourseId { get; set; }

		public string Status { get; set; }

		public int? Score { get; set; }

		public DateTime StartTime { get; set; }

		public DateTime? EndTime { get; set; }

		public virtual Course Course { get; set; }

		public virtual User User { get; set; }
	}
}
