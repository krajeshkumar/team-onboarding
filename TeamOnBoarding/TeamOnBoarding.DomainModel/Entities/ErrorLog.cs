﻿using System;
using TeamOnBoarding.DomainModel.Common;

namespace TeamOnBoarding.DomainModel.Entities
{
    public class ErrorLog : GlobalEntity
    {
        public string Message { get; set; }

        public string CauseTypeName { get; set; }

        public string StackTrace { get; set; }

        public Nullable<DateTime> LogTime { get; set; }
    }
}