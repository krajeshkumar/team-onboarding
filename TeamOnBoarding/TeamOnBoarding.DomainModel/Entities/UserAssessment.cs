using System.ComponentModel.DataAnnotations.Schema;
using TeamOnBoarding.DomainModel.Common;
namespace TeamOnBoarding.DomainModel.Entities
{
	public class UserAssessment : GlobalEntity
	{
		[NotMapped]
		public int Id { get; set; }

		public int UserId { get; set; }

		public int QuestionId { get; set; }

		public int? Score { get; set; }

		public string UserAnswer { get; set; }

		public virtual Question Question { get; set; }

		public virtual User User { get; set; }
	}
}