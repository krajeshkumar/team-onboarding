using System;
using System.Collections.Generic;
using TeamOnBoarding.DomainModel.Common;

namespace TeamOnBoarding.DomainModel.Entities
{
	public class Track : GlobalEntity
	{
		public Track()
		{
			this.Courses = new HashSet<Course>();
			this.Users = new HashSet<User>();
		}

		public string TrackName { get; set; }

		public DateTime? CreatedDate { get; set; }

		public DateTime? ModifiedDate { get; set; }

		public bool IsActive { get; set; }

		public virtual ICollection<Course> Courses { get; set; }

		public virtual ICollection<User> Users { get; set; }
	}
}