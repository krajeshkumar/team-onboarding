using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using TeamOnBoarding.DomainModel.Common;

namespace TeamOnBoarding.DomainModel.Entities
{
    public class Option : GlobalEntity
    {
        public string OptionDescription { get; set; }

        public int QuestionId { get; set; }

        [ForeignKey("QuestionId")]
        public virtual Question Question { get; set; }
    }
}