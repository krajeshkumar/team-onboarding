using System;
using System.Collections.Generic;
using TeamOnBoarding.DomainModel.Common;

namespace TeamOnBoarding.DomainModel.Entities
{
	public class Course : GlobalEntity
	{
		public Course()
		{
			this.Questions = new HashSet<Question>();
			this.UserAssessmentStatus = new HashSet<UserAssessmentStatus>();
		}

		public string CourseName { get; set; }

		public string Content { get; set; }

		public Nullable<DateTime> CreatedDate { get; set; }

		public Nullable<DateTime> ModifiedDate { get; set; }

		public int? CourseTypeId { get; set; }

		public int TrackId { get; set; }

		public bool IsActive { get; set; }

		public virtual CourseType CourseType { get; set; }

		public virtual Track Track { get; set; }

		public virtual ICollection<Question> Questions { get; set; }

		public virtual ICollection<UserAssessmentStatus> UserAssessmentStatus { get; set; }
	}
}