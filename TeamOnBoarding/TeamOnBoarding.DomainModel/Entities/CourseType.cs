using System;
using System.Collections.Generic;
using TeamOnBoarding.DomainModel.Common;

namespace TeamOnBoarding.DomainModel.Entities
{
    public class CourseType : GlobalEntity
    {
        public CourseType()
        {
            this.Courses = new HashSet<Course>();
        }

        public string CourseTypeName { get; set; }

        public Nullable<int> Duration { get; set; }

        public virtual ICollection<Course> Courses { get; set; }
    }
}