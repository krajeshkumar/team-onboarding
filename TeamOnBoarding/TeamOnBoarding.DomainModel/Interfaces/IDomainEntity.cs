﻿using TeamOnBoarding.Database.Common.Enums;

namespace TeamOnBoarding.DomainModel.Interfaces
{
    public interface IDomainEntity
    {
        int Id { get; set; }

        DomainObjectState ObjectState { get; set; }
    }
}