﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using TeamOnBoarding.Database.Common.Enums;
using TeamOnBoarding.Database.Common.Objects;
using TeamOnBoarding.DomainModel.Interfaces;

namespace TeamOnBoarding.DomainModel.Common
{
    [Serializable]
    public class GlobalEntity : IDomainEntity
    {
        public int Id { get; set; }

        [NotMapped]
        public DomainObjectState ObjectState { get; set; }

        [NotMapped]
        public List<ValidationMessage> Messages { get; set; }
    }
}