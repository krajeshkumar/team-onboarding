﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeamOnBoarding.DomainModel.Entities;

namespace TeamOnBoarding.Database.DataLayer.Configuration
{
	public class UserAssessmentStatusMap  : EntityTypeConfiguration<UserAssessmentStatus>
	{
		public UserAssessmentStatusMap()
		{
			ToTable("UserAssessmentStatus");
			HasKey(lb => new { lb.UserId, lb.CourseId });
		}
	}
}
