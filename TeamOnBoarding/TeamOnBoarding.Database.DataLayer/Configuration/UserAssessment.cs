﻿using System.Data.Entity.ModelConfiguration;
using TeamOnBoarding.DomainModel.Entities;

namespace TeamOnBoarding.Database.DataLayer.Configuration
{
    public class UserAssessmentMap : EntityTypeConfiguration<UserAssessment>
    {
        public UserAssessmentMap()
        {
            ToTable("UserAssessment");
            HasKey(lb => new { lb.UserId, lb.QuestionId });
        }
    }
}