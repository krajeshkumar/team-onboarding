﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeamOnBoarding.DomainModel.Entities;

namespace TeamOnBoarding.Database.DataLayer.Configuration
{
    public class QuestionMap : EntityTypeConfiguration<Question>
    {
        public QuestionMap()
        {
            ToTable("Question");
            HasKey(lb => lb.Id);
        }
    }
}
