﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeamOnBoarding.DomainModel.Entities;

namespace TeamOnBoarding.Database.DataLayer.Configuration
{
    public class CourseTypeMap : EntityTypeConfiguration<CourseType>
    {
        public CourseTypeMap()
        {
            ToTable("CourseType");
            HasKey(lb => lb.Id);
        }
    }
}
