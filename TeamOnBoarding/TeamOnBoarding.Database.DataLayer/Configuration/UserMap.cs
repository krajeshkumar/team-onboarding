﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeamOnBoarding.DomainModel.Entities;

namespace TeamOnBoarding.Database.DataLayer.Configuration
{
    public class UserMap : EntityTypeConfiguration<User>
    {
        public UserMap()
        {
            ToTable("User");
            HasKey(lb => lb.Id);
        }
    }
}
