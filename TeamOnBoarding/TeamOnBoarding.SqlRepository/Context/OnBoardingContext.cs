﻿using System.Data.Entity;
using TeamOnBoarding.Database.DataLayer.Configuration;
using TeamOnBoarding.DomainModel.Entities;

namespace TeamOnBoarding.SqlRepository.Context
{
	public class OnBoardingContext : BaseContext
	{
		public OnBoardingContext()
			: base("name=OnboardingConnectionString")
		{
		}

		public DbSet<Course> Courses { get; set; }

		public DbSet<CourseType> CourseTypes { get; set; }

		public DbSet<Option> Options { get; set; }

		public DbSet<Question> Questions { get; set; }

		public DbSet<Track> Tracks { get; set; }

		public DbSet<User> Users { get; set; }

		public DbSet<UserAssessment> UserAssessments { get; set; }

		public DbSet<UserAssessmentStatus> UserAssessmentStatus { get; set; }

		public DbSet<ErrorLog> ErrorLog { get; set; }

		#region Model Binding

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Configurations.Add(new CourseMap());
			modelBuilder.Configurations.Add(new CourseTypeMap());
			modelBuilder.Configurations.Add(new OptionMap());
			modelBuilder.Configurations.Add(new QuestionMap());
			modelBuilder.Configurations.Add(new TrackMap());
			modelBuilder.Configurations.Add(new UserMap());
			modelBuilder.Configurations.Add(new UserAssessmentMap());
			modelBuilder.Configurations.Add(new UserAssessmentStatusMap());
			modelBuilder.Configurations.Add(new ErrorLogMap());

			base.OnModelCreating(modelBuilder);
		}

		#endregion Model Binding
	}
}