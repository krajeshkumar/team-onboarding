﻿using System;
using System.Data.Entity;
using TeamOnBoarding.Database.Common.Enums;
using TeamOnBoarding.DomainModel.Interfaces;

namespace TeamOnBoarding.SqlRepository.Context
{
    public class BaseContext : DbContext
    {
        protected Guid _userGroupId { get; set; }

        protected BaseContext(string connectionString)
            : base(connectionString)
        {
        }

        public void SetAdded(object entity)
        {
            this.Entry(entity).State = EntityState.Added;
        }

        public void SetDeleted(object entity)
        {
            this.Entry(entity).State = EntityState.Deleted;
        }

        public void Save()
        {
            this.SaveChanges();
        }

        public void SetModified(object entity)
        {
            if (entity is IDomainEntity)
            {
                Type entityType = entity.GetType();
                var commissionEntity = ((IDomainEntity)entity);

                if (commissionEntity.Id != 0)
                {
                    var set = this.Set(entityType);
                    if (set != null)
                    {
                        var attachedEntity = set.Find(commissionEntity.Id);
                        if (attachedEntity != null)
                        {
                            var attachedCommissionEntity = (IDomainEntity)attachedEntity;
                            attachedCommissionEntity.ObjectState = DomainObjectState.Modified;

                            var attachedEntry = this.Entry(attachedEntity);
                            attachedEntry.CurrentValues.SetValues(entity);

                            entity = attachedEntity;
                        }
                    }
                }
            }

            this.Entry(entity).State = EntityState.Modified;
        }
    }
}