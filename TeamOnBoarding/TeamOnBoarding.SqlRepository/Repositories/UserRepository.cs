﻿using System;
using System.Collections.Generic;
using System.Linq;
using TeamOnBoarding.DomainModel.Entities;

namespace TeamOnBoarding.SqlRepository.Repositories
{
	public class UserRepository : BaseRepository<User>
	{
		public UserRepository()
			: base()
		{
		}

		public bool GetLoginStatus(string username, string password)
		{
			//var user = context.Set<User>().Where(x => x.Username == username && x.Password == password).ToList().SingleOrDefault();
			var user = AllIncluding(x => x.Username == username && x.Password == password).ToList().SingleOrDefault();
			return user != null;
		}

		public User GetUserByUserName(string username)
		{
			//var user = context.Set<User>().Where(x => x.Username == username).ToList().SingleOrDefault();
			var user = AllIncluding(x => x.Username == username).ToList().SingleOrDefault();
			return user;
		}

		public IEnumerable<User> GetAll()
		{
			return All().Where(x => x.IsActive).ToList();
		}

		public IEnumerable<User> GetUserByPaging(int pageSize, int currentPage)
		{
			return All().OrderBy(x => x.FirstName).Skip((currentPage - 1) * pageSize).Take(pageSize);
		}

		public bool AddUser(string firstName, string lastName, string email, string userName, string gender, string role, string password, int trackId)
		{
			var userExists = AllIncluding(x => x.Username == userName).SingleOrDefault();
			if (userExists != null)
			{
				userExists.FirstName = firstName;
				userExists.LastName = lastName;
				userExists.EmailAddress = email;
				userExists.Role = role;
				userExists.Username = userName;
				userExists.Password = password;
				userExists.IsActive = true;
				userExists.CreatedDate = DateTime.Now;
				userExists.ModifiedDate = DateTime.Now;
				var updatedUser = Update(userExists);
				if (trackId != 0)
				{
					userExists.TrackId = trackId;
				}
				return updatedUser.Messages == null;
			}
			var user = new User
			{
				FirstName = firstName,
				LastName = lastName,
				EmailAddress = email,
				Role = role,
				Username = userName,
				Password = password,
				IsActive = true,
				CreatedDate = DateTime.Now
			};
			if (trackId!=0)
			{
				user.TrackId = trackId;
			}
			var savedUser = Add(user);
			if (savedUser.Messages != null)
			{
				return false;
			}
			return true;
		}

		public bool UpdateUser(int id, string firstName, string lastName, string email, string userName, string gender, int trackId)
		{
			var user = GetById(id);
			user.FirstName = firstName;
			user.LastName = lastName;
			user.EmailAddress = email;
			user.Gender = gender;
			user.Username = userName;
			user.ModifiedDate = DateTime.Now;
			if (user.Role == "user")
			{
				user.TrackId = trackId;
			}
			var updatedUser = Update(user);
			if (updatedUser.Messages != null)
			{
				return false;
			}
			return true;
		}

		public bool DeleteUser(int userId)
		{
			var user = GetById(userId);
			user.IsActive = false;
			user.ModifiedDate = DateTime.Now;
			var updatedUser = Update(user);
			return updatedUser.Messages == null;
		}
	}
}