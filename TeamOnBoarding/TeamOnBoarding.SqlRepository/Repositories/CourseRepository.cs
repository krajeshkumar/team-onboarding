﻿using System;
using System.Collections.Generic;
using System.Linq;
using TeamOnBoarding.DomainModel.Classes;
using TeamOnBoarding.DomainModel.Common;
using TeamOnBoarding.DomainModel.Entities;

using entity = TeamOnBoarding.DomainModel.Entities;
using model = TeamOnBoarding.DomainModel.Classes;

namespace TeamOnBoarding.SqlRepository.Repositories
{
	public class CourseRepository : BaseRepository<entity.Course>
	{
		private readonly TrackRepository _trackRepository;

		public CourseRepository()
		{
			_trackRepository = new TrackRepository();
		}

		public IEnumerable<T> GetAll<T>() where T : GlobalEntity
		{
			return All<T>().ToList();
		}

		public IEnumerable<entity.Course> GetCoursesByTrack(int trackId)
		{
			var courses = AllIncluding(x => x.IsActive && x.TrackId == trackId).ToList();
			return courses;
		}

		public IEnumerable<entity.Course> GetBasicTrackCourses()
		{
			var basicTrack = _trackRepository.GetBasicTrack();
			var courses = All().Where(x => x.TrackId == basicTrack.Id).ToList();
			return courses;
		}

		public string GetCourseContent(int courseId)
		{
			var course = GetById(courseId);
			return course != null ? course.Content : string.Empty;
		}

		public bool UpdateCourseContent(int courseId, string content)
		{
			try
			{
				var course = GetById(courseId);
				course.Content = content;
				course.ModifiedDate = DateTime.Now;
				var updatedCourse = Update(course);
				return updatedCourse.Messages == null;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public bool AddCourse(int trackId, string courseName)
		{
			try
			{
				var courseExist = AllIncluding(x => x.CourseName == courseName).SingleOrDefault();
				if (courseExist!=null)
				{
					courseExist.IsActive = true;
					courseExist.CreatedDate = DateTime.Now;
					courseExist.ModifiedDate = DateTime.Now;
					var courseUpdated = Update(courseExist);
					return courseUpdated.Messages == null;
				}
				var course = new entity.Course
				{
					CourseName = courseName,
					TrackId = trackId,
					IsActive = true,
					CreatedDate = DateTime.Now
				};
				var savedCourse = Add(course);
				return savedCourse.Messages == null;
			}
			catch (Exception)
			{
				throw;
			}
		}

		public bool DeleteCourse(int id)
		{
			try
			{
				var course = GetById(id);
				course.IsActive = false;
				course.ModifiedDate = DateTime.Now;
				var courseModified = Update(course);
				return courseModified.Messages == null;
			}
			catch (Exception)
			{
				throw;
			}
		}

		public bool UpdateCourse(int id, string courseName)
		{
			var course = GetById(id);
			course.CourseName = courseName;
			course.ModifiedDate = DateTime.Now;
			var updatedCourse = Update(course);
			return updatedCourse.Messages == null;
		}

		public bool AddQuestion(model.QuestionModel questionmodel)
		{
			try
			{
			    var question = new entity.Question
			    {
			        QuestionType = questionmodel.QuestionType,
			        QuestionDescription = questionmodel.QuestionTitle,
			        CourseId = questionmodel.CourseId,
			        Time = questionmodel.QuestionTime
			    };
			    var savedQuestion = Add<entity.Question>(question);
				var questionId = savedQuestion.Id;
			    if (questionmodel.Options != null)
			    {
			        foreach (var optionModel in questionmodel.Options)
			        {
			            var option = new entity.Option {OptionDescription = optionModel.OptionTitle, QuestionId = questionId};
			            var savedOption = Add<entity.Option>(option);
			        }
			        return savedQuestion.Messages == null;
			    }
			    return savedQuestion.Messages == null;
			}
			catch (Exception)
			{
				throw;
			}
		}

		public bool UpdateQuestion(model.QuestionModel questionmodel)
		{
			try
			{
				var questionId = questionmodel.QuestionId;
				var question = AllIncluding<Question>(x => x.Id == questionId).FirstOrDefault();
				question.QuestionDescription = questionmodel.QuestionTitle;
				question.QuestionType = questionmodel.QuestionType;
				question.Time = questionmodel.QuestionTime;
				var savedQuestion = Update<entity.Question>(question);
				var questionOptions = AllIncluding<entity.Option>(x => x.QuestionId == questionId).ToList();
				foreach (var option in questionOptions)
				{
					Delete<entity.Option>(option);
				}
				if (questionmodel.Options != null)
				{
					foreach (var optionModel in questionmodel.Options)
					{
						var option = new entity.Option();
						option.OptionDescription = optionModel.OptionTitle;
						option.QuestionId = questionId;
						var savedOption = Add<entity.Option>(option);
					}
				}
				return savedQuestion.Messages == null;
			}
			catch (Exception)
			{
				throw;
			}
		}

		public bool DeleteQuestion(int questionId)
		{
			try
			{
				var question = AllIncluding<Question>(x => x.Id == questionId).FirstOrDefault();
				Delete<entity.Question>(question);
				return true;
			}
			catch (Exception)
			{
				throw;
			}
		}

		public List<QuestionModel> GetQuestionsByCourse(int courseId)
		{
			var questions = (from q in Context.Questions
							 join option in Context.Options
							 on q.Id equals option.QuestionId into ojoin
							 from subset in ojoin.DefaultIfEmpty()
							 where q.CourseId == courseId
							 select new
							 {
								 questionId = q.Id,
								 questionDescription
									 = q.QuestionDescription,
								 questionType = q.QuestionType,
								 questionTime = q.Time,
								 optionId = (subset == null ? -1 : subset.Id),
								 optionDescription = (subset == null ? string.Empty : subset.OptionDescription)
							 }).ToList();
			var distinctQuestions = questions.Select(x => x.questionId).Distinct();
			var questionModelList = new List<QuestionModel>();
			foreach (var question in distinctQuestions)
			{
				var questionModel = new QuestionModel();
				questionModel.CourseId = courseId;
				var selectedQuestions = questions.Where(x => x.questionId == question).ToList();
				foreach (var selectedQuestion in selectedQuestions)
				{
					if (selectedQuestion.optionId != -1)
					{
						questionModel.Options.Add(new model.Option { OptionTitle = selectedQuestion.optionDescription, OptionId = selectedQuestion.optionId });
					}
					questionModel.QuestionId = selectedQuestion.questionId;
					questionModel.QuestionTitle = selectedQuestion.questionDescription;
					questionModel.QuestionTime = selectedQuestion.questionTime;
					questionModel.QuestionType = selectedQuestion.questionType;
				}
				questionModelList.Add(questionModel);
			}
			return questionModelList;
		}
	}
}