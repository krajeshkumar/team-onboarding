﻿using System;
using System.Collections.Generic;
using System.Linq;
using TeamOnBoarding.DomainModel.DTO;
using TeamOnBoarding.DomainModel.Entities;
using TeamOnBoarding.Library.Common.Enum;

using entity = TeamOnBoarding.DomainModel.Entities;

namespace TeamOnBoarding.SqlRepository.Repositories
{
    public class UserAssessmentRepository : BaseRepository<entity.UserAssessment>
    {
        public bool AddUserAssessmentStatus(int courseId, int userId, UserAssessmentStatusType statusType)
        {
            var isUserAssessmentExist =
                AllIncluding<UserAssessmentStatus>(x => x.UserId == userId && x.CourseId == courseId).Any();
            if (isUserAssessmentExist) return false;
            try
            {
                var userAssessmentStatus = new UserAssessmentStatus
                {
                    CourseId = courseId,
                    UserId = userId,
                    Status = statusType.ToString(),
                    StartTime = DateTime.Now
                };
                var statusAdded = Add<UserAssessmentStatus>(userAssessmentStatus);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool SubmitTest(List<QuestionResponseDto> questionResponse, int userId, int courseId)
        {
            using (var dbContextTransaction = Context.Database.BeginTransaction())
            {
                try
                {
                    foreach (var response in questionResponse)
                    {
                        var userAssessment = new UserAssessment
                        {
                            UserId = userId,
                            QuestionId = response.QuestionId,
                            UserAnswer = response.Response
                        };
                        var insertResult = Add<UserAssessment>(userAssessment);
                        if (insertResult.Messages != null && insertResult.Messages.Count > 0)
                        {
                            throw new Exception();
                        }
                    }
                    var userAssessmentStatus =
                        AllIncluding<UserAssessmentStatus>(x => x.CourseId == courseId && x.UserId == userId)
                            .FirstOrDefault();
                    if (userAssessmentStatus != null)
                    {
                        userAssessmentStatus.Status = UserAssessmentStatusType.Submitted.ToString();
                        userAssessmentStatus.EndTime = DateTime.Now;
                        Update(userAssessmentStatus);
                    }
                    else
                    {
                        dbContextTransaction.Rollback();
                        return false;
                    }
                    dbContextTransaction.Commit();
                    return true;
                }
                catch (Exception)
                {
                    dbContextTransaction.Rollback();
                    return false;
                }
            }
        }

        public UserAssessmentStatus GetUserAssessmentStatus(int userId, int courseId)
        {
            var userAssessmentStatus =
                AllIncluding<UserAssessmentStatus>(x => x.UserId == userId && x.CourseId == courseId).FirstOrDefault();
            return userAssessmentStatus;
        }

    }
}