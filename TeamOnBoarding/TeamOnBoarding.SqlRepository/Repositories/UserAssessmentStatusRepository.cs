﻿using System;
using System.Collections.Generic;
using System.Linq;
using TeamOnBoarding.DomainModel.Entities;
using TeamOnBoarding.Library.Common.Enum;
using entity = TeamOnBoarding.DomainModel.Entities;

namespace TeamOnBoarding.SqlRepository.Repositories
{
    public class UserAssessmentStatusRepository: BaseRepository<entity.UserAssessmentStatus>
    {
        public List<User> GetUsersToBeEvaluated()
        {
            var userAssessmentNotEvaluated = AllIncluding(x => x.Status != UserAssessmentStatusType.Evaluated.ToString()).Select(x =>x.UserId).ToList();
            var users = AllIncluding<User>(x => userAssessmentNotEvaluated.Contains(x.Id)).ToList();
            return users;
        }
    }
}
