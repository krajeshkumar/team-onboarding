﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using TeamOnBoarding.Database.Common.Objects;
using TeamOnBoarding.DomainModel.Common;
using TeamOnBoarding.SqlRepository.Context;
using TeamOnBoarding.SqlRepository.Helpers;

namespace TeamOnBoarding.SqlRepository.Repositories
{
	public class BaseRepository<T> where T : GlobalEntity
	{
	    public OnBoardingContext Context { get; }

	    public BaseRepository()
		{
			Context = new OnBoardingContext();
		}

		public IQueryable<T> All()
		{
			return Context.Set<T>();
		}

		public IQueryable<TU> All<TU>() where TU : GlobalEntity
		{
			return Context.Set<TU>();
		}

		public IQueryable<T> AllIncluding(Expression<Func<T, bool>> predicate)
		{
			return All().Where(predicate);
		}

		public IQueryable<TU> AllIncluding<TU>(Expression<Func<TU, bool>> predicate) where TU : GlobalEntity
		{
			return All<TU>().Where(predicate);
		}

		public void Erase(List<T> eraseList)
		{
			foreach (var item in eraseList)
			{
				Context.Set<T>().Attach(item);
				Context.Set<T>().Remove(item);
				Context.SaveChanges();
			}
		}

		public T GetById(int id)
		{
			return All().SingleOrDefault(x => x.Id == id);
		}

		public void Save()
		{
			Context.SaveChanges();
		}

		public T Add(T entity)
		{
			try
			{
				this.Context.SetAdded(entity);
				this.Context.ApplyStateChanges();
				Context.SaveChanges();
				if (Context != null)
					((DbContext)Context).MarkEntitiesAsUpdated();
			}
			catch (DbEntityValidationException dbEx)
			{
				foreach (var entityErr in dbEx.EntityValidationErrors)
				{
					foreach (var error in entityErr.ValidationErrors)
					{
						entity.Messages.Add(new ValidationMessage(Guid.NewGuid(), error.PropertyName, error.ErrorMessage));
					}
				}
			}
			return entity;
		}

		public TU Add<TU>(TU entity) where TU : GlobalEntity
		{
			try
			{
				this.Context.SetAdded(entity);
				this.Context.ApplyStateChanges();
				Context.SaveChanges();
				if (Context != null)
					((DbContext)Context).MarkEntitiesAsUpdated();
			}
			catch (DbEntityValidationException dbEx)
			{
				foreach (var entityErr in dbEx.EntityValidationErrors)
				{
					foreach (var error in entityErr.ValidationErrors)
					{
						entity.Messages.Add(new ValidationMessage(Guid.NewGuid(), error.PropertyName, error.ErrorMessage));
					}
				}
			}
			return entity;
		}

		public T Update(T entity)
		{
			try
			{
				this.Context.SetModified(entity);
				this.Context.ApplyStateChanges();
				Context.SaveChanges();
				if (Context != null)
					((DbContext)Context).MarkEntitiesAsUpdated();
			}
			catch (DbEntityValidationException dbEx)
			{
				foreach (var error in dbEx.EntityValidationErrors.SelectMany(entityErr => entityErr.ValidationErrors))
				{
				    entity.Messages.Add(new ValidationMessage(Guid.NewGuid(), error.PropertyName, error.ErrorMessage));
				}
			}
			return entity;
		}

		public TU Update<TU>(TU entity) where TU : GlobalEntity
		{
			try
			{
				this.Context.SetModified(entity);
				this.Context.ApplyStateChanges();
				Context.SaveChanges();
				if (Context != null)
					((DbContext)Context).MarkEntitiesAsUpdated();
			}
			catch (DbEntityValidationException dbEx)
			{
				foreach (var error in dbEx.EntityValidationErrors.SelectMany(entityErr => entityErr.ValidationErrors))
				{
				    entity.Messages.Add(new ValidationMessage(Guid.NewGuid(), error.PropertyName, error.ErrorMessage));
				}
			}
			return entity;
		}

		public TU Delete<TU>(TU entity) where TU : GlobalEntity
		{
			try
			{
				this.Context.SetDeleted(entity);
				this.Context.ApplyStateChanges();
				Context.SaveChanges();
				if (Context != null)
					((DbContext)Context).MarkEntitiesAsUpdated();
			}
			catch (DbEntityValidationException dbEx)
			{
				foreach (var error in dbEx.EntityValidationErrors.SelectMany(entityErr => entityErr.ValidationErrors))
				{
				    entity.Messages.Add(new ValidationMessage(Guid.NewGuid(), error.PropertyName, error.ErrorMessage));
				}
			}
			return entity;
		}
	}
}