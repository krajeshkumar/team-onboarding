﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using TeamOnBoarding.DomainModel.Entities;

namespace TeamOnBoarding.SqlRepository.Repositories
{
	public class TrackRepository : BaseRepository<Track>
	{
		public TrackRepository()
			: base()
		{
		}

		public IEnumerable<Track> GetAll()
		{
			return AllIncluding(x => x.IsActive).ToList();
		}

		public Track GetBasicTrack()
		{
			return All().Where(x => x.TrackName == "Basic").SingleOrDefault();
		}

		public bool AddTrack(string trackName)
		{
			var track = new Track
			{
				TrackName = trackName,
				CreatedDate = DateTime.Now,
				IsActive = true
			};
			var savedTrack = Add(track);
			return savedTrack.Messages != null;
		}

		public bool UpdateTrack(int id, string trackName)
		{
			var track = GetById(id);
			track.TrackName = trackName;
			track.ModifiedDate = DateTime.Now;
			var updatedTrack = Update(track);
			return updatedTrack.Messages == null;
		}

		public bool DeleteTrack(int id)
		{
			using (DbContextTransaction dbTran = Context.Database.BeginTransaction())
			{
				try
				{
					var track = GetById(id);
					var coursesByTrack = AllIncluding<Course>(x => x.TrackId == id).ToList();
					var courseDeleted = true;
					foreach (var course in coursesByTrack)
					{
						course.IsActive = false;
						course.ModifiedDate = DateTime.Now;
						var updatedCourse = Update<Course>(course);
						if (updatedCourse.Messages != null)
						{
							courseDeleted = false;
							break;
						}
					}
					if (!courseDeleted)
					{
						dbTran.Rollback();
						return false;
					}
					track.IsActive = false;	
					track.ModifiedDate = DateTime.Now;
					var updatedTrack = Update(track);
					dbTran.Commit();
					if (updatedTrack.Messages != null)
					{
						dbTran.Rollback();
						return false;
					}
					return true;
				}
				catch (Exception ex)
				{
					throw ex;
				}
			}
		}
	}
}