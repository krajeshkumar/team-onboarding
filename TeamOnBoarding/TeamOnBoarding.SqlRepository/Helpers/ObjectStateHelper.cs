﻿using System.Data.Entity;
using TeamOnBoarding.Database.Common.Enums;

namespace TeamOnBoarding.SqlRepository.Helpers
{
    public static class ObjectStateHelper
    {
        public static DomainObjectState ToEntityFrameworkState(this EntityState state)
        {
            switch (state)
            {
                case EntityState.Added:
                    return DomainObjectState.Added;

                case EntityState.Modified:
                    return DomainObjectState.Modified;
            }

            return DomainObjectState.Unchanged;
        }
    }
}