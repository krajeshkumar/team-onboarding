﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeamOnBoarding.Database.Common.Enums;
using TeamOnBoarding.DomainModel.Interfaces;

namespace TeamOnBoarding.SqlRepository.Helpers
{
    public static class DbContextExtensions
    {
        public static void ApplyStateChanges(this DbContext context)
        {
            foreach (var entry in context.ChangeTracker.Entries<IDomainEntity>())
            {
                IDomainEntity entity = entry.Entity;
                entity.ObjectState = entry.State.ToEntityFrameworkState();
            }
        }

        public static void MarkEntitiesAsUpdated(this DbContext context)
        {
            foreach (var entry in context.ChangeTracker.Entries<IDomainEntity>())
            {
                IDomainEntity entity = entry.Entity;
                entity.ObjectState = DomainObjectState.Unchanged;
            }
        }
    }
}
