﻿namespace TeamOnBoarding.Database.Common.Enums
{
    public enum DomainObjectState
    {
        Unchanged,
        Added,
        Modified,
        Deleted
    }
}