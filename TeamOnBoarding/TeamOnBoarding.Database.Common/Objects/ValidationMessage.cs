﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamOnBoarding.Database.Common.Objects
{
    public class ValidationMessage
    {
        public Guid Id { get; set; }
        public ValidationMessage(Guid Id, string propertyName, string message)
        {
            this.Id = Id;
            this.PropertyName = propertyName;
            this.Message = message;
        }
        public string PropertyName { get; set; }
        public string Message { get; set; }
    }
}
