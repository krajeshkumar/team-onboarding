﻿namespace TeamOnBoarding.Library.Common.DTO
{
	public class TimeDTO
	{
		public int Minute { get; set; }

		public int Seconds { get; set; }
	}
}