﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamOnBoarding.Library.Common.Enum
{
	public enum UserAssessmentStatusType
	{
		NotStarted,
		Started,
		InProgress,
		Submitted,
        Evaluated
	}
}
