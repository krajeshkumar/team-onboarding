﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeamOnBoarding.Library.Common.DTO;

namespace TeamOnBoarding.Library.Common
{
    public static class DataConversion
    {
        public static TimeDTO ConvertSecondsToMinute(int timeInSeconds)
        {
            var minutes = timeInSeconds / 60;
            var seconds = timeInSeconds % 60;
            return new TimeDTO { Minute = minutes, Seconds = seconds };
        }
    }
}
