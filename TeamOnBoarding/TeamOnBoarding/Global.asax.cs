﻿using System;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using TeamOnBoarding.SqlRepository.Repositories;

namespace TeamOnBoarding
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();
        }

        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
        {
            if (FormsAuthentication.CookiesSupported)
            {
                if (Request.Cookies[FormsAuthentication.FormsCookieName] != null)
                {
                    try
                    {
                        var username = FormsAuthentication.Decrypt(Request.Cookies[FormsAuthentication.FormsCookieName].Value).Name;
                        var userRepository = new UserRepository();
                        var user = userRepository.GetUserByUserName(username);
                        var roles = user.Role;

                        HttpContext.Current.User = new System.Security.Principal.GenericPrincipal(
                                                new System.Security.Principal.GenericIdentity(username, "Forms"),
                                                roles.Split(';'));
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }
        }
    }
}