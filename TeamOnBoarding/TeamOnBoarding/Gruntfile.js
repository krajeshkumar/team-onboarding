﻿'use strict';

module.exports = function (grunt) {
	// Configurable paths for the application
	var appConfig = {
		app: require('./bower.json').appPath || 'app',
		dist: 'dist',
		nuget: 'Scripts'
	};

	// Define the configuration for all the tasks
	grunt.initConfig({
		// Project settings
		symmetry: appConfig,

		concat: {
			options: {
				sourceMap: true
			},
			app: {
				src: [
					'<%= symmetry.app %>/scripts/{,**/}*.js'
				],
				dest: '<%= symmetry.app %>/symmetryapp.js'
			},
			nuget: {
				src: [
					'<%= symmetry.nuget %>/jquery.signalR-2.2.0.js',
					'<%= symmetry.nuget %>/linq.js'
				],
				dest: '<%= symmetry.app %>/nuget.js'
			}
		},

		bower_concat: {
			all: {
				dest: 'app/plugins.js',
				cssDest: 'Assets/CSS/plugins.css',
				exclude: ['jquery', 'modernizr'],
				dependencies: {
					'ang-accordion': 'angular'
				},
				bowerOptions: {
					relative: false
				}
			}
		},

		copy: {
			plugins: {
				files: [
					{ expand: true, flatten: true, src: ['bower_components/font-awesome/fonts/**'], dest: 'Assets/Fonts/', filter: 'isFile' },
					{ expand: true, flatten: true, src: ['bower_components/bootstrap/fonts/**'], dest: 'Assets/Fonts/', filter: 'isFile' },
					{ expand: true, flatten: true, src: ['bower_components/angular-utils-pagination/dirPagination.tpl.html'], dest: 'app/scripts/directives/', filter: 'isFile' }
				]
			}
		},

		uglify: {
			app: {
				options: {
					sourceMap: true,
					sourceMapIncludeSources: true,
					sourceMapIn: '<%= symmetry.app %>/symmetryapp.js.map'
				},
				files: {
					'<%= symmetry.app %>/symmetryapp.min.js': ['<%= symmetry.app %>/symmetryapp.js']
				}
			},
			plugins: {
				files: {
					'<%= symmetry.app %>/plugins.min.js': ['<%= symmetry.app %>/plugins.js']
				}
			},
			nuget: {
				files: {
					'<%= symmetry.app %>/nuget.min.js': ['<%= symmetry.app %>/nuget.js']
				}
			}
		},

		cssmin: {
			options: {
				shorthandCompacting: false,
				roundingPrecision: -1
			},
			plugins: {
				files: {
					'Assets/CSS/plugins.min.css': 'Assets/CSS/plugins.css'
				}
			}
		},

		watch: {
			files: ['Assets/CSS/partials/*', '<%= symmetry.app %>/scripts/{,**/}*.js'],
			tasks: ['concat', 'less', 'uglify:app']
		}
	});

	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-bower-concat');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.registerTask('default', ['bower_concat', 'copy', 'concat', 'cssmin', 'uglify', 'watch']);
};