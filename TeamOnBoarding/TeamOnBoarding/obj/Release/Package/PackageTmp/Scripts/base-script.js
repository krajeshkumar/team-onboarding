﻿$(document).ready(function () {
    var nav = function () {
        $('.gw-nav > li > a').click(function () {
            var gw_nav = $('.gw-nav');
            gw_nav.find('li').removeClass('active');
            $('.gw-nav > li > ul > li').removeClass('active');

            var checkElement = $(this).parent();
            var ulDom = checkElement.find('.gw-submenu')[0];

            if (ulDom == undefined) {
                checkElement.addClass('active');
                $('.gw-nav').find('li').find('ul:visible').slideUp();
                return;
            }
            if (ulDom.style.display != 'block') {
                gw_nav.find('li').find('ul:visible').slideUp();
                gw_nav.find('li.init-arrow-up').removeClass('init-arrow-up').addClass('arrow-down');
                gw_nav.find('li.arrow-up').removeClass('arrow-up').addClass('arrow-down');
                checkElement.removeClass('init-arrow-down');
                checkElement.removeClass('arrow-down');
                checkElement.addClass('arrow-up');
                checkElement.addClass('active');
                checkElement.find('ul.gw-submenu').slideDown(300);
            } else {
                checkElement.removeClass('init-arrow-up');
                checkElement.removeClass('arrow-up');
                checkElement.removeClass('active');
                checkElement.addClass('arrow-down');
                checkElement.find('ul').slideUp(300);
            }
        });
        $('.gw-nav > li > ul > li > a').click(function () {
            var checkElement = $(this).parent();
            var ulDom = checkElement.find('ul')[0];
            checkElement.parent().parent().removeClass('active');
            $('.gw-nav > li > ul > li').removeClass('active');
            checkElement.addClass('active');
            if (ulDom == undefined) {
                checkElement.addClass('active');
                checkElement.parent().find('ul:visible').slideUp();
                return;
            }
            if (ulDom.style.display != 'block') {
                checkElement.parent().find('ul:visible').slideUp();
                checkElement.parent().find('li.init-arrow-up').removeClass('init-arrow-up').addClass('arrow-down');
                checkElement.parent().find('li.arrow-up').removeClass('arrow-up').addClass('arrow-down');
                checkElement.removeClass('init-arrow-down');
                checkElement.removeClass('arrow-down');
                checkElement.addClass('arrow-up');
                checkElement.addClass('active');
                checkElement.find('ul').slideDown(300);
            } else {
                checkElement.removeClass('init-arrow-up');
                checkElement.removeClass('arrow-up');
                checkElement.removeClass('active');
                checkElement.addClass('arrow-down');
                checkElement.find('ul').slideUp(300);
            }
        });
    };
    nav();

    $(".user").click(function () {
        var windowWidth = $(window).width();
        var userdropdown = $("#user-dropdown");
        var userdropdownWidth =  userdropdown.width();
        userdropdown.css("left", windowWidth - userdropdownWidth);
        userdropdown.css("display", "block").css("opacity", "1").css("top", "55px").css("overflow-y", "auto");
    });

    var headers = ["H1", "H2", "H3", "H4", "H5", "H6"];
    $(".accordion").click(function (e) {
        var target = e.target,
            name = target.nodeName.toUpperCase();

        if ($.inArray(name, headers) > -1) {
            var subItem = $(target).next();

            //slideUp all elements (except target) at current depth or greater
            var depth = $(subItem).parents().length;
            var allAtDepth = $(".accordion p, .accordion div").filter(function () {
                if ($(this).parents().length >= depth && this !== subItem.get(0)) {
                    return true;
                }
            });
            $(allAtDepth).slideUp("fast");

            //slideToggle target content and adjust bottom border if necessary
            subItem.slideToggle("fast", function () {
                $(".accordion :visible:last").css("border-radius", "0 0 10px 10px");
            });
            $(target).css({ "border-bottom-right-radius": "0", "border-bottom-left-radius": "0" });
        }
    });

    $(document).mouseup(function (e) {
        var container = $("#user-dropdown");

        if (!container.is(e.target) // if the target of the click isn't the container...
            && container.has(e.target).length === 0) // ... nor a descendant of the container
        {
            container.hide();
        }
    });
});