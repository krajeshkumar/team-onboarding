﻿using System;
using System.Collections.Generic;
using System.Linq;
using web = System.Web;
using System.Web.Mvc;
using TeamOnBoarding.SqlRepository.Repositories;
using TeamOnBoarding.Models;

namespace TeamOnBoarding.Controllers
{
    [Authorize]
    public class HomeController : BaseController
    {
        private readonly CourseRepository _courseRepository;

        public HomeController()
        {
            _courseRepository = new CourseRepository();
        }
                
        public ActionResult Index()
        {
            return View(HomeLayoutModel);
        }
        
        public JsonResult GetContent(int courseId)
        {
            var content = _courseRepository.GetCourseContent(courseId);
            return new JsonResult() { Data = content, MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

    }
}
