﻿using System.Web.Mvc;
using TeamOnBoarding.DomainModel.Classes;
using TeamOnBoarding.Models;
using TeamOnBoarding.SqlRepository.Repositories;

namespace TeamOnBoarding.Controllers
{
    [Authorize(Roles = "admin")]
    public class AssessmentController : BaseController
    {
        private readonly CourseRepository _courseRepository;
        private readonly CourseModel _courseModel;

        public AssessmentController()
        {
            _courseRepository = new CourseRepository();
            _courseModel = new CourseModel(HomeLayoutModel);
        }

        public ActionResult Index(int courseId)
        {
            var course = _courseRepository.GetById(courseId);
            _courseModel.CourseId = course.Id;
            _courseModel.CourseName = course.CourseName;
            return View(_courseModel);
        }

        public JsonResult GetQuestions(int courseId)
        {
            var questions = _courseRepository.GetQuestionsByCourse(courseId);
            return new JsonResult() { Data = questions, MaxJsonLength = int.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpPost]
        public JsonResult SaveQuestion(QuestionModel questionmodel)
        {
            var result = _courseRepository.AddQuestion(questionmodel);
            return new JsonResult() { Data = result, MaxJsonLength = int.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [HttpPost]
        public JsonResult UpdateQuestion(QuestionModel questionmodel)
        {
            var result = _courseRepository.UpdateQuestion(questionmodel);
            return new JsonResult() { Data = result, MaxJsonLength = int.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult DeleteQuestion(int questionId)
        {
            var result = _courseRepository.DeleteQuestion(questionId);
            return new JsonResult() { Data = result, MaxJsonLength = int.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
    }
}