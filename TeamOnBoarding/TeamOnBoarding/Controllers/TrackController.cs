﻿using System;
using System.Linq;
using System.Web.Mvc;
using TeamOnBoarding.DomainModel.Entities;
using TeamOnBoarding.SqlRepository.Repositories;

namespace TeamOnBoarding.Controllers
{
	[Authorize]
	public class TrackController : BaseController
	{
		private readonly TrackRepository _trackRepository;
		private readonly CourseRepository _courseRepository;

		public TrackController()
		{
			_trackRepository = new TrackRepository();
			_courseRepository = new CourseRepository();
		}

		[Authorize(Roles = "admin")]
		public ActionResult Index()
		{
			return View(HomeLayoutModel);
		}

		public JsonResult GetAllTrack()
		{
			var tracks = _trackRepository.GetAll();
			var trackList = tracks.Select(x => new { trackName = x.TrackName, trackId = x.Id });
			return new JsonResult() { Data = trackList, MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
		}

		public JsonResult GetCourseType()
		{
			var courseType = _courseRepository.GetAll<CourseType>();
			var courseTypeList = courseType.Select(x => new { courseTypeName = x.CourseTypeName, courseTypeId = x.Id });
			return new JsonResult() { Data = courseTypeList, MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
		}

		[Authorize(Roles = "admin")]
		public JsonResult AddTrack(string trackName)
		{
			var isSuccess = _trackRepository.AddTrack(trackName);
			return new JsonResult() { Data = isSuccess, MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
		}

		[Authorize(Roles = "admin")]
		public JsonResult AddCourse(int trackId, string courseName)
		{
			var isSuccess = _courseRepository.AddCourse(trackId, courseName);
			return new JsonResult() { Data = isSuccess, MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
		}

		[Authorize(Roles = "admin")]
		public JsonResult UpdateTrack(int id, string trackName)
		{
			var isSuccess = _trackRepository.UpdateTrack(id, trackName);
			return new JsonResult() { Data = isSuccess, MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
		}

		[Authorize(Roles = "admin")]
		public JsonResult UpdateCourse(int id, string courseName)
		{
			var isSuccess = _courseRepository.UpdateCourse(id, courseName);
			return new JsonResult() { Data = isSuccess, MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
		}

		[Authorize(Roles = "admin")]
		public JsonResult GetCourses(int trackId)
		{
			var courses = _courseRepository.GetCoursesByTrack(trackId);
			var courseList = courses.Select(x => new { courseId = x.Id, courseName = x.CourseName, courseType = x.CourseTypeId });
			return new JsonResult() { Data = courseList, MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
		}

		public JsonResult GetContent(int courseId)
		{
			var content = _courseRepository.GetCourseContent(courseId);
			return new JsonResult() { Data = content, MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
		}

		[Authorize(Roles = "admin")]
		public JsonResult UpdateContent(int courseId, string content)
		{
			var result = _courseRepository.UpdateCourseContent(courseId, content);
			return new JsonResult() { Data = result, MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
		}

		[Authorize(Roles = "admin")]
		public JsonResult DeleteTrack(int trackId)
		{
			var result = _trackRepository.DeleteTrack(trackId);
			return new JsonResult() { Data = result, MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
		}

		[Authorize(Roles = "admin")]
		public JsonResult DeleteCourse(int courseId)
		{
			var result = _courseRepository.DeleteCourse(courseId);
			return new JsonResult() { Data = result, MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
		}
	}
}