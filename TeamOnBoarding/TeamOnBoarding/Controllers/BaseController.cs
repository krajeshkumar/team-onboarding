﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TeamOnBoarding.DomainModel.Entities;
using TeamOnBoarding.SqlRepository.Repositories;
using model = TeamOnBoarding.Models;
using web = System.Web;

namespace TeamOnBoarding.Controllers
{
    public class BaseController : Controller
    {
        private readonly UserRepository _userRepository;
        private readonly CourseRepository _courseRepository;
        private readonly UserAssessmentStatusRepository _userAssessmentStatusRepository;

        public model.HomeLayoutModel HomeLayoutModel { get; }

        public BaseController()
        {
            _userRepository = new UserRepository();
            _courseRepository = new CourseRepository();
            _userAssessmentStatusRepository = new UserAssessmentStatusRepository();
            HomeLayoutModel = GetModel();
        }

        private model.HomeLayoutModel GetModel()
        {
            var currentUser = web.HttpContext.Current.User;
            var userName = currentUser.Identity.Name;
            var user = _userRepository.GetUserByUserName(userName);
            if (user == null)
            {
                return new model.HomeLayoutModel();
            }
            var homeLayoutModel = new model.HomeLayoutModel()
            {
                UserId = user.Id,
                Role = user.Role,
                FirstName = user.FirstName,
                LastName = user.LastName,
                UserName = user.Username
            };
            if (user.Role == "admin")
            {
                var allTracks = _courseRepository.GetAll<Track>();
                var allTrackCourseList = (from track in allTracks
                    let trackCourses = _courseRepository.GetCoursesByTrack(track.Id)
                    select new model.TrackCourse
                    {
                        TrackName = track.TrackName, Courses = trackCourses.Select(x => new model.Course {CourseId = x.Id, CourseName = x.CourseName}).ToList()
                    }).ToList();
                homeLayoutModel.TrackWithCourseDetails = allTrackCourseList;
                homeLayoutModel.EvaluationList = GetUsersToBeEvaluated();
                return homeLayoutModel;
            }
            var userTrack = user.Track;
            var basicCourses = _courseRepository.GetBasicTrackCourses();
            var basicTrackCourseList = new List<model.Course>();
            basicCourses.ToList()
                .ForEach(x => basicTrackCourseList.Add(new model.Course {CourseId = x.Id, CourseName = x.CourseName}));
            homeLayoutModel.TrackWithCourseDetails = new List<model.TrackCourse>
            {
                new model.TrackCourse {TrackName = "Basic", Courses = basicTrackCourseList}
            };
            if (userTrack.TrackName != "Basic")
            {
                var courses = _courseRepository.GetCoursesByTrack(userTrack.Id);
                var trackCourseList = new List<model.Course>();
                courses.ToList()
                    .ForEach(x => trackCourseList.Add(new model.Course {CourseId = x.Id, CourseName = x.CourseName}));
                homeLayoutModel.TrackWithCourseDetails.Add(new model.TrackCourse
                {
                    TrackName = userTrack.TrackName,
                    Courses = trackCourseList
                });
            }

            return homeLayoutModel;
        }

        private List<model.User> GetUsersToBeEvaluated()
        {
            var userToBeEvaluated = _userAssessmentStatusRepository.GetUsersToBeEvaluated();
            var users =
                userToBeEvaluated.Select(x => new model.User {UserId = x.Id, UserName = x.FirstName + " " + x.LastName}).ToList();
            return users;
        }
    }
}