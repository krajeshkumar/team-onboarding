﻿using System;
using System.Web.Mvc;
using System.Web.Security;
using TeamOnBoarding.Models;
using TeamOnBoarding.SqlRepository.Repositories;

namespace TeamOnBoarding.Controllers
{
    public class LoginController : Controller
    {
        private UserRepository _userRepository;

        public LoginController()
        {
            _userRepository = new UserRepository();
        }

        public ActionResult Index(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginModel userModel, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var userValid = _userRepository.GetLoginStatus(userModel.UserName, userModel.Password);
                if (userValid)
                {
                    FormsAuthentication.SetAuthCookie(userModel.UserName, false);
                    if (Url.IsLocalUrl(returnUrl) && returnUrl.Length>1 && returnUrl.StartsWith("/") && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\") )
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "The username or password provided is incorrect");
                }
            }
			else
			{
				ModelState.AddModelError("", "Fill in the required fields.");
			}

            return View("Index",userModel);
        }

        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Login");
        }
    }
}