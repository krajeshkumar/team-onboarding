﻿using System;
using System.Linq;
using System.Web.Mvc;
using TeamOnBoarding.Models;
using TeamOnBoarding.SqlRepository.Repositories;
using web = System.Web;

namespace TeamOnBoarding.Controllers
{
    [Authorize(Roles = "admin")]
    public class UserController : BaseController
    {
        private readonly UserRepository _userRepository;
        private CourseRepository _courseRepository;

        public UserController()
        {
            _userRepository = new UserRepository();
            _courseRepository = new CourseRepository();
        }

        public ActionResult Index()
        {
            return View(HomeLayoutModel);
        }

        public JsonResult GetUsers(int pageSize, int currentPage)
        {
            var userLists = _userRepository.GetAll();
            var users = userLists.OrderBy(x => x.FirstName).Skip((currentPage - 1) * pageSize).Take(pageSize).Select(x => new { userid = x.Id, username = x.Username, firstname = x.FirstName, lastname = x.LastName, gender = x.Gender, email = x.EmailAddress, active = x.IsActive, role = x.Role, trackName = x.Track!=null?x.Track.TrackName:string.Empty, trackId = x.TrackId });
            var totalSize = userLists.Count();
            return new JsonResult() { Data = new { users = users, totalSize = totalSize }, MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult AddUser(UserModel user)
        {
            var isSuccess = _userRepository.AddUser(user.FirstName, user.LastName, user.Email, user.UserName, user.Gender, user.Role, user.Password, user.TrackId);
            return new JsonResult() { Data = isSuccess, MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult UpdateUser(UserModel user)
        {
            var isSuccess = _userRepository.UpdateUser(user.Id, user.FirstName, user.LastName, user.Email, user.UserName, user.Gender, user.TrackId);
            return new JsonResult() { Data = isSuccess, MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

		public JsonResult DeleteUser(int userId)
        {
            var isSuccess = _userRepository.DeleteUser(userId);
            return new JsonResult() { Data = isSuccess, MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

    }
}