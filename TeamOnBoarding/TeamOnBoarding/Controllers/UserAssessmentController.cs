﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using TeamOnBoarding.DomainModel.DTO;
using TeamOnBoarding.Library.Common;
using TeamOnBoarding.Library.Common.Enum;
using TeamOnBoarding.Models;
using TeamOnBoarding.SqlRepository.Repositories;

namespace TeamOnBoarding.Controllers
{
	[Authorize(Roles="user")]
	public class UserAssessmentController : BaseController
	{
		private readonly CourseRepository _courseRepository;
		private readonly UserAssessmentRepository _userAssessmentRepository;
		private readonly UserAssessmentModel _userAssessmentModel;

		public UserAssessmentController()
		{
			_courseRepository = new CourseRepository();
			_userAssessmentRepository = new UserAssessmentRepository();
			_userAssessmentModel = new UserAssessmentModel(HomeLayoutModel);
		}

		public ActionResult Index(int courseId)
		{
			var userAssessmentStatus = _userAssessmentRepository.GetUserAssessmentStatus(_userAssessmentModel.UserId, courseId);
			var course = _courseRepository.GetById(courseId);
			_userAssessmentModel.CourseId = course.Id;
			_userAssessmentModel.CourseName = course.CourseName;
			_userAssessmentModel.UserAssessmentStatus = UserAssessmentStatusType.NotStarted.ToString();
			if (userAssessmentStatus != null)
			{
				_userAssessmentModel.UserAssessmentStatus = userAssessmentStatus.Status;
				return View(_userAssessmentModel);
			}
			var questions = _courseRepository.GetQuestionsByCourse(courseId);
			var totalTime = DataConversion.ConvertSecondsToMinute(questions.Select(x => x.QuestionTime).Sum());
			_userAssessmentModel.TotalTime = totalTime;
			_userAssessmentModel.QuestionCount = questions.Count;
			return View(_userAssessmentModel);
		}

		public JsonResult GetQuestions(int courseId)
		{
			var questions = _courseRepository.GetQuestionsByCourse(courseId);
			return new JsonResult() { Data = questions, MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
		}

		public JsonResult SetUserAssessmentStatus(int courseId)
		{
			var response = _userAssessmentRepository.AddUserAssessmentStatus(courseId, _userAssessmentModel.UserId, UserAssessmentStatusType.Started);
			return new JsonResult() { Data = response, MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
		}

		public JsonResult SubmitTest(List<QuestionResponseModel> questionResponse, int courseId)
		{
			Mapper.CreateMap<QuestionResponseModel, QuestionResponseDto>();
			var questionResponseDto = Mapper.Map<List<QuestionResponseModel>, List<QuestionResponseDto>>(questionResponse);
			var submitStatus = _userAssessmentRepository.SubmitTest(questionResponseDto, _userAssessmentModel.UserId, courseId);
			return new JsonResult() { Data = submitStatus, MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
		}
	}
}