﻿using System.Web.Mvc;
using TeamOnBoarding.Models;
using TeamOnBoarding.SqlRepository.Repositories;

namespace TeamOnBoarding.Controllers
{
	[Authorize(Roles = "user")]
	public class UserCourseController : BaseController
	{
	    private readonly CourseRepository _courseRepository;

		public UserCourseController()
		{
			_courseRepository = new CourseRepository();
		}

		public ActionResult Index(int courseId)
		{
			var content = _courseRepository.GetCourseContent(courseId);
			var course = _courseRepository.GetById(courseId);
			var userCourseModel = new UserCourseModel(HomeLayoutModel) { CourseId = courseId, CourseContent = content, CourseName = course.CourseName };
			return View(userCourseModel);
		}
	}
}