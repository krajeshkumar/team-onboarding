﻿using System.Web.Mvc;
using TeamOnBoarding.DomainModel.Classes;
using TeamOnBoarding.Models;
using TeamOnBoarding.SqlRepository.Repositories;

namespace TeamOnBoarding.Controllers
{
    [Authorize(Roles = "admin")]
    public class EvaluationController : BaseController
    {
        private readonly CourseRepository _courseRepository;
        private readonly CourseModel _courseModel;

        public EvaluationController ()
        {
            _courseRepository = new CourseRepository();
            _courseModel = new CourseModel(HomeLayoutModel);
        }

        public ActionResult Index(int courseId)
        {
            var course = _courseRepository.GetById(courseId);
            _courseModel.CourseId = course.Id;
            _courseModel.CourseName = course.CourseName;
            return View(_courseModel);
        }
    }
}
