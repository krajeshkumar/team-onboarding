﻿using System.Collections.Generic;

namespace TeamOnBoarding.Models
{
	public class HomeLayoutModel
	{
		public int UserId { get; set; }

		public string Role { get; set; }

		public string FirstName { get; set; }

		public string LastName { get; set; }

		public string UserName { get; set; }

		public List<User> EvaluationList { get; set; }

		public List<TrackCourse> TrackWithCourseDetails { get; set; }

		public HomeLayoutModel()
		{
		}

		public HomeLayoutModel(HomeLayoutModel homeLayoutModel)
		{
			this.UserId = homeLayoutModel.UserId;
			this.FirstName = homeLayoutModel.FirstName;
			this.LastName = homeLayoutModel.LastName;
			this.Role = homeLayoutModel.Role;
			this.UserName = homeLayoutModel.UserName;
			this.TrackWithCourseDetails = homeLayoutModel.TrackWithCourseDetails;
		}
	}

	public class TrackCourse
	{
		public string TrackName { get; set; }

		public List<Course> Courses { get; set; }
	}

	public class Course
	{
		public string CourseName { get; set; }

		public int CourseId { get; set; }
	}
        
	public class User
	{
		public string UserName { get; set; }

		public int UserId { get; set; }
	}
}