﻿namespace TeamOnBoarding.Models
{
    public class UserModel
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string UserName { get; set; }

        public string Gender { get; set; }

        public string Role { get; set; }

        public string Email { get; set; }

        public bool IsActive { get; set; }

        public string Password { get; set; }

        public int TrackId { get; set; }
    }
}