﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeamOnBoarding.Models
{
	public class QuestionResponseModel
	{
		public int QuestionId { get; set; }
		public string Response { get; set; }
		public string QuestionType { get; set; }
	}
}