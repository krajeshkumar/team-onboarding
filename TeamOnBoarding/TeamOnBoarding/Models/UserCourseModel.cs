﻿namespace TeamOnBoarding.Models
{
    public class UserCourseModel : HomeLayoutModel
    {
        public UserCourseModel(HomeLayoutModel homeLayoutModel)
            : base(homeLayoutModel)
        {
        }

        public int CourseId { get; set; }

        public string CourseName { get; set; }

        public string CourseContent { get; set; }
    }
}