﻿using TeamOnBoarding.Library.Common.Enum;
namespace TeamOnBoarding.Models
{
	public class UserAssessmentModel : CourseModel
	{
		public string UserAssessmentStatus { get; set; }

		public UserAssessmentModel(HomeLayoutModel homeLayoutModel)
			: base(homeLayoutModel)
		{
		}
	}
}