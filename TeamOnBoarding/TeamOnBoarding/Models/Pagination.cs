﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeamOnBoarding.Models
{
    public class Pagination
    {
        public int PageSize { get; set; }
        public int CurrentPage { get; set; }
    }
}