﻿using System.Collections.Generic;
using TeamOnBoarding.Library.Common.DTO;

namespace TeamOnBoarding.Models
{
    public class CourseModel : HomeLayoutModel
    {
        public int CourseId { get; set; }

        public string CourseName { get; set; }

        public int QuestionCount { get; set; }

        public TimeDTO TotalTime { get; set; }

        public List<Question> Questions { get; set; }

        public CourseModel(HomeLayoutModel homeLayoutModel)
            : base(homeLayoutModel)
        {
        }
    }

    public class Question
    {
        public int QuestionId { get; set; }

        public string Description { get; set; }

        public string QuestionType { get; set; }

        public List<Option> Options { get; set; }
    }

    public class Option
    {
        public string Description { get; set; }

        public int OptionId { get; set; }
    }
}