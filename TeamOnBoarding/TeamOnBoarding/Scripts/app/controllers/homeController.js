﻿(function () {
	'use strict';

	angular.module("BaseModule").controller("HomeController", homeModule);

	function homeModule($rootScope, $http, baseService) {
		var scope = this;
		scope.gender = "null";
		scope.role = "null";
		scope.modalAlertEdit = false;
		scope.courseName = "";
		$rootScope.$on("courseSelectionChanged", function () {
			var courseContent = baseService.getCourseContent();
			scope.content = courseContent.content;
			scope.courseName = courseContent.courseName;
		});
	}
})();