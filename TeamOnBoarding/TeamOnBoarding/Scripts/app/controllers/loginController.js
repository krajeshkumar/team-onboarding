﻿(function () {
	'use strict';

	angular.module("LoginAdministration").controller("LoginController", loginModule);

	function loginModule($scope, $http, loginService) {
		$scope.showAlert = false;  //Alert message enable/disable

		$scope.login = function () {
			var username = $scope.username;
			var password = $scope.password;
			loginService.getLogin(username, password).then(function (response) {

			}, function (error) {

			});
		};

		//show alert message
		$scope.showMessage = function (showAlert, message, alertMessageType) {
			if (showAlert === false) {
				$scope.showAlert = !$scope.showAlert;
			}
			$scope.alertType = alertMessageType;
			switch (alertMessageType) {
				case alertType[1]:
					$scope.alertClass = "alert-success";
					$scope.alertModalClass = "alert-success";
					break;;
				case alertType[2]:
					$scope.alertClass = "alert-warning";
					$scope.alertModalClass = "alert-warning";
					break;
				case alertType[3]:
					$scope.alertClass = "alert-error";
					$scope.alertModalClass = "alert-error";
					break;
				default:
			}
			$scope.message = message;
			$scope.productGroupAlterMessage = message;
		}

		$scope.switchBool = function (value) {
			$scope[value] = !$scope[value];
		};

	}
})();