﻿(function () {
	'use strict';

	angular.module("BaseModule").controller("userGridViewController", ["$scope", "$http", "userService", function ($scope, $http, userService) {
		$scope.editColumn = '<div style="margin-top:5px; padding-left:5px; float: left;" title="edit user"><a class="mdi-image-edit edit_link" ng-click="editUser(row)"  data-toggle="modal" data-target="#editUserModal" ></a></div>' +
			'<div style="margin-top:5px; padding-left:5px; float: left;" title="delete user"><a class="mdi-action-delete delete_link" ng-click="deleteUser(row)"></a></div>'

		$scope.gridColumns = [
							{ field: 'userid', visible: false },
							{ field: 'firstname', visible: false },
							{ field: 'lastname', visible: false },
							{ field: 'gender', visible: false },
							{ displayName: 'Name', cellTemplate: '<div class="ngCellText">{{row.entity.firstname}} {{row.entity.lastname}}</div>', sortable: false, resizable: true },
							{ field: 'email', displayName: 'Email Id', resizable: true },
							{ field: 'username', displayName: 'Username', resizable: true },
							{ field: 'role', displayName: 'Role' },
							{ field: 'trackName', displayName: 'Track' },
							{ field: 'trackId', visible: false },
							{ field: 'active', displayName: 'Status' },
							{ displayName: "Edit", cellTemplate: $scope.editColumn, sortable: false, resizable: true }];
		$scope.totalServerItems = 0;
		$scope.pagingOptions = {
			pageSizes: [5, 10, 100],
			pageSize: 5,
			currentPage: 1
		}

		$scope.editUser = function (row) {
			userService.setUserDetails(row.entity.userid, row.entity.firstname, row.entity.lastname, row.entity.username, row.entity.email, row.entity.role, row.entity.active, row.entity.gender, row.entity.trackId);
			$scope.$emit("userEditModal", null)
		}

		$scope.deleteUser = function (row) {
			userService.deleteUser(row.entity.userid).then(function (response) {
				if (response) {
					//scope.showModalMessage('modalAddUserAlert', 'AddUserMessage', "User added successfully", alertType[1]);
				}
				else {
					//scope.showModalMessage('modalAddUserAlert', 'AddUserMessage', "User could not be added", alertType[1]);
				}
				$scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
			}, function (error) {
				scope.showModalMessage('modalAddUserAlert', 'AddUserMessage', "User could not be added.", alertType[3]);
			})
		}
		$scope.filterOptions = {
			filterText: "",
			useExternalFilter: false
		};

		$scope.totalServerItems = 0;
		//Grid configuration
		$scope.gridDataForUser = {
			data: "fieldsOfGrid",
			showGroupPanel: false,
			enablePaging: true,
			showFooter: true,
			multiSelect: false,
			totalServerItems: "totalServerItems",
			enableRowSelection: false,
			columnDefs: "gridColumns",
			rowTemplate: '<div ng-dblclick="onDblClickRow(row)" ng-style="{ \’cursor\’: row.cursor }" ng-repeat="col in renderedColumns" ng-class="col.colIndex()" class="ngCell {{col.cellClass}}"><div class="ngVerticalBar" ng-style="{height: rowHeight}" ng-class="{ ngVerticalBarVisible: !$last }">&nbsp;</div><div ng-cell></div></div>',
			footerTemplate: "gridFooter.html",
			pagingOptions: $scope.pagingOptions,
			filterOptions: $scope.filterOptions
		};

		$scope.fieldsOfGrid = [];

		$scope.init = function () {
			$scope.getPagedDataAsync($scope.pagingOptions.pageSize, 1);
		};

		$scope.getPagedDataAsync = function (pageSize, page) {
			setTimeout(function () {
				userService.getUserGridData(pageSize, page).then(function (response) {
					$scope.fieldsOfGrid = response.users;
					$scope.totalServerItems = response.totalSize;
				}, function (errorResponse) { console.log(errorResponse); });
			}, 100);
		};

		//keeping watch on page value selected
		$scope.$watch('pagingOptions', function (newVal, oldVal) {
			if (newVal !== oldVal || newVal.currentPage !== oldVal.currentPage) {
				$scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
				if (newVal.pageSize !== oldVal.pageSize) {
					$scope.pagingOptions.currentPage = 1;
				}
			}
		}, true);

		$scope.$on("refreshGrid", function (event) {
			$scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
		})
	}]);
})();