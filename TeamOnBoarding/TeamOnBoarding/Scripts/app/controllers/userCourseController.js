﻿(function () {
	'use strict';

	angular.module("BaseModule").controller("UserCourseController", userCourseModule);

	function userCourseModule(trackService) {
		var scope = this;
		scope.content = "string is empty";
		var emptyContent = '<p><span class="center-light-font">No content available.</span></p>';
		scope.init = function (courseId) {
			trackService.getContent(courseId).then(function (response) {
				scope.content = response != "" ? response : emptyContent;
			}, function (error) {
			});
		};
	}
})();