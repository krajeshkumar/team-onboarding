﻿(function () {
	'use strict';

	angular.module("BaseModule").controller("UserController", userModule);

	function userModule($scope, $http, $timeout, userService, trackService, constantsService, alertType) {
		var scope = this;
		scope.gender = "null";
		scope.role = "null";
		scope.modalAlertEdit = false;
		scope.modalAlert = false;
		scope.modalAddUserAlert = false;
		scope.modalEditUserAlert = false;
		scope.roles = null;
		scope.init = init;
		scope.showlpgaModal = showlpgaModal;
		scope.onRoleChange = onRoleChange;
		scope.addUser = addUser;
		scope.updateUser = updateUser;
		scope.showModalMessage = showModalMessage;
		scope.switchBool = switchBool;
		$scope.$on("userEditModal", function (event, args) {
			var userDetails = userService.getUserDetails();
			scope.userIdEdit = userDetails.userid;
			scope.firstNameEdit = userDetails.firstname;
			scope.lastNameEdit = userDetails.lastname;
			scope.userNameEdit = userDetails.username;
			scope.emailEdit = userDetails.email;
			scope.roleEdit = userDetails.role;
			scope.isActiveEdit = userDetails.isactive;
			scope.genderEdit = userDetails.gender;
			scope.editUserName = userDetails.firstname + ' ' + userDetails.lastname;
			if (userDetails.role == "user") {
				scope.showTrack = true;
			}
			else {
				scope.showTrack = false;
			}
			scope.trackEdit = userDetails.trackId;
		})

		function init() {
			scope.roles = constantsService.getRoles();
			trackService.getAllTrack().then(function (response) {
				scope.tracks = response;
			}, function (error) {
			})
		};

		function showlpgaModal(addUserForm) {
			scope.showTrack = false;
			scope.gender = "null";
			scope.role = "null";
			userService.resetFormFieldControls(addUserForm);
		};

		function onRoleChange() {
			var selectedRole = scope.role;
			scope.track = "";
			if (scope.role == "user") {
				scope.showTrack = true;
			}
			else {
				scope.showTrack = false;
			}
		};

		function addUser(form) {
			if (!form.$valid) {
				$("#addUserForm .ng-pristine.ng-invalid").addClass("ng-dirty").removeClass("ng-pristine");
				return false;
			}
			var firstName = scope.firstName;
			var lastName = scope.lastName;
			var email = scope.email;
			var userName = scope.userName;
			var gender = scope.gender;
			var role = scope.role;
			var password = scope.password;
			var track = scope.track;
			if (gender === 'null' ) {
				scope.showModalMessage('modalAddUserAlert', 'AddUserMessage', "Please select a gender ", alertType[2]);
				return;
			}
			if (role === 'null') {
				scope.showModalMessage('modalAddUserAlert', 'AddUserMessage', "Please select a role ", alertType[2]);
				return;
			}
			userService.addUser(firstName, lastName, email, userName, gender, role, password, track).then(function (response) {
				if (response) {
					scope.showModalMessage('modalAddUserAlert', 'AddUserMessage', "User added successfully", alertType[1]);
				}
				else {
					scope.showModalMessage('modalAddUserAlert', 'AddUserMessage', "User could not be added", alertType[1]);
				}
				$scope.$broadcast("refreshGrid")
			}, function (error) {
				scope.showModalMessage('modalAddUserAlert', 'AddUserMessage', "User could not be added.", alertType[3]);
			})
		};

		function updateUser(form) {
			if (!form.$valid) {
				$("#editUserForm .ng-pristine.ng-invalid").addClass("ng-dirty").removeClass("ng-pristine");
				return false;
			}
			var firstName = scope.firstNameEdit;
			var lastName = scope.lastNameEdit;
			var email = scope.emailEdit;
			var userName = scope.userNameEdit;
			var gender = scope.genderEdit;
			var track = scope.trackEdit;
			if (gender === 'null') {
				scope.showModalMessage('modalEditUserAlert', 'EditUserMessage', "Please select a gender ", alertType[2]);
				return;
			}
			userService.updateUser(scope.userIdEdit, firstName, lastName, email, userName, gender, track).then(function (response) {
				$scope.$broadcast("refreshGrid")
				scope.showModalMessage('modalEditUserAlert', 'EditUserMessage', "User updated successfully", alertType[1],10000);
			}, function (error) {
				scope.showModalMessage('modalEditUserAlert', 'EditUserMessage', "User could not be updated.", alertType[3]);
			})
		};

		//show alert message in Modal
		function showModalMessage(modalAlert, alterMessage, message, alertMessageType, timeOut) {
			timeOut = timeOut || 1000;
			if (scope[modalAlert] === false) {
				scope[modalAlert] = !scope[modalAlert];
			}
			//scope.alertType = alertMessageType;
			switch (alertMessageType) {
				case alertType[1]:
					scope.alertModalClass = "alert-success";
					break;;
				case alertType[2]:
					scope.alertModalClass = "alert-warning";
					break;
				case alertType[3]:
					scope.alertModalClass = "alert-error";
					break;
				default:
			}
			scope[alterMessage] = message;
			$timeout(function () {
				scope[modalAlert] = false;
			}, timeOut);
		};

		function switchBool(value) {
			scope[value] = false;//!scope[value];
		};

	}
})();