﻿(function () {
	'use strict';

	angular.module("BaseModule").controller("AssessmentController", assessmentModule);

	assessmentModule.$inject = ["$http", "$timeout", "assessmentService", "alertType"];

	function assessmentModule($http, $timeout, assessmentService, alertType) {
		var scope = this;
		scope.modalAlert = false;
		scope.options = [];
		scope.value = 1;
		scope.questionType = "";
		scope.courseId = "";
		scope.saveType = "";
		scope.showQuestionModal = showQuestionModal;
		scope.ShowQuestionModalForEdit = ShowQuestionModalForEdit;
		scope.questionTypeChange = questionTypeChange;
		scope.addNewOption = addNewOption;
		scope.removeOption = removeOption;
		scope.saveQuestion = saveQuestion;
		scope.deleteQuestion = deleteQuestion;
		scope.setRequiredFieldStyle = setRequiredFieldStyle;
		scope.showModalMessage = showModalMessage;
		scope.switchBool = switchBool;

		scope.init = function (courseId) {
			assessmentService.getQuestions(courseId).then(function (response) {
				if (response.length > 0) {
					scope.questions = response;
					scope.QuestionEmpty = false;
				}
				else {
					scope.QuestionEmpty = true;
				}
			}, function (error) {
			});
		};

		function showQuestionModal(form) {
			CKEDITOR.instances.questionTitle.setData(null);
			scope.questionType = null;
			scope.questionTime = null;
			scope.modalAlert = false;
			scope.saveType = "add";
			assessmentService.resetFormFieldControls(form);
		};

		function ShowQuestionModalForEdit(event, form) {
			assessmentService.resetFormFieldControls(form);
			var divClicked = $(event.currentTarget);
			scope.saveType = "update";
			var questionTitle = divClicked.parents(".question-panel").find(".questiontitle").html();
			scope.questionType = divClicked.parents(".question-panel").find(".question-type").val().trim();
			scope.questionTime = divClicked.parents(".question-panel").find(".question-time").val().trim();
			scope.questionId = divClicked.parents(".question-panel").find(".question-id").val().trim();
			CKEDITOR.instances.questionTitle.setData(questionTitle);
			var options = divClicked.parents(".question-panel").find(".question-detail-option");
			scope.options.splice(0, scope.options.length);
			options.each(function (index, val) {
				var optionTitle = $(val).text().trim();
				var optionId = $(val).find(".option-id").val();
				scope.options.push({ "name": optionTitle, "value": optionId });
			});

			scope.modalAlert = false;
		};

		function questionTypeChange() {
			var questionType = scope.questionType;
			scope.options.splice(0, scope.options.length);
			if (questionType != "multiplechoice") {
				scope.value = 1;
			}
		};

		function addNewOption() {
			scope.options.push({ "name": "", "value": scope.value++ });
		};

		function removeOption(optionId) {
			scope.options = $.grep(scope.options, function (n, i) {
				return n.value != optionId;
			});
		};

		function saveQuestion(form) {
			if (!form.$valid) {
				$("#questionForm .ng-pristine.ng-invalid").addClass("ng-dirty").removeClass("ng-pristine");
				return false;
			}
			var courseId = scope.courseId;
			var questionTitle = CKEDITOR.instances.questionTitle.getData();
			var questionType = scope.questionType;
			var questionTime = scope.questionTime;
			var model = { questionId: null, courseId: courseId, questionTitle: questionTitle, questionType: questionType, questionTime: questionTime, options: [] };
			if (scope.saveType == "update") {
				var questionId = scope.questionId;
				model.questionId = questionId;
			}
			var options = [];
			$.each(scope.options, function (index, obj) {
				options.push({ optionTitle: obj.name, isCorrectOption: false });
			});
			model.options = options;
			assessmentService.saveQuestion(model, scope.saveType).then(function (response) {
				if (response) {
					scope.showModalMessage('modalAlert', 'questionMessage', scope.saveType == "add" ? "Question added successfully." : "Question updated successfully.", alertType[1]);
					scope.init(scope.courseId);
				}
				else {
					scope.showModalMessage('modalAlert', 'questionMessage',  scope.saveType == "add" ? "Question could not be added." : "Question could not be updated.", alertType[3]);
				}
			}, function (error) {
				scope.showModalMessage('modalAlert', 'questionMessage',  scope.saveType == "add" ? "Question could not be added." : "Question could not be updated.", alertType[3]);
			});
		};

		function deleteQuestion(event, questionId) {
			assessmentService.deleteQuestion(questionId).then(function (response) {
				if (response) {
					scope.showModalMessage('modalAlert', 'questionMessage',  "Question updated successfully.", alertType[1]);
					var divClicked = $(event.currentTarget);
					var parentDOM = divClicked.parents(".question-panel");
					parentDOM.fadeOut("slow", function () {
						parentDOM.remove();
						if ($(".question-panel").length <= 0) {
							scope.init(scope.courseId);
						}
					});
				}
				else {
					scope.showModalMessage('modalAlert', 'questionMessage',  "Question could not be updated.", alertType[3]);
				}
			}, function (error) {
				scope.showModalMessage('modalAlert', 'questionMessage',  "Question could not be updated.", alertType[3]);
			});
		};

		function setRequiredFieldStyle(element) {
			element.css("border-bottom", "2px solid red");
		};

		function showMessage(modalAlert, message, alertMessageType) {
			if (modalAlert === false) {
				scope.modalAlert = !scope.modalAlert;
			}
			switch (alertMessageType) {
				case alertType[1]:
					scope.alertModalClass = "alert-success";
					break;;
				case alertType[2]:
					scope.alertModalClass = "alert-warning";
					break;
				case alertType[3]:
					scope.alertModalClass = "alert-error";
					break;
				default:
			}
			scope.questionMessage = message;
		}

		//show alert message in Modal
		function showModalMessage(modalAlert, alterMessage, message, alertMessageType, timeOut) {
			timeOut = timeOut || 3000;
			if (scope[modalAlert] === false) {
				scope[modalAlert] = !scope[modalAlert];
			}
			//scope.alertType = alertMessageType;
			switch (alertMessageType) {
				case alertType[1]:
					scope.alertModalClass = "alert-success";
					break;;
				case alertType[2]:
					scope.alertModalClass = "alert-warning";
					break;
				case alertType[3]:
					scope.alertModalClass = "alert-error";
					break;
				default:
			}
			scope[alterMessage] = message;
			$timeout(function () {
				scope[modalAlert] = false;
			}, timeOut);
		};

		function switchBool(value) {
			scope[value] = !scope[value];
		};
	}
})();