﻿(function () {
	'use strict';

	angular.module("BaseModule").controller("TrackController", trackModule);

	function trackModule($http, $timeout, trackService, alertType) {
		var scope = this;
		scope.trackName = null;
		scope.modalTrackAlert = false;
		scope.modalCourseAlert = false;
		scope.trackId = null;
		scope.isAddCourse = true;
		scope.showCourse = false;
		scope.CourseEmpty = false;
		scope.courseSelected = false;
		var emptyContent = '<p><span style="font-size:16px"><span style="color:#696969"><span style="font-family:arial,helvetica,sans-serif">Content is empty. Click here to add content.</span></span></span></p>'
		scope.init = init;
		scope.loadTracks = loadTracks;
		scope.getCourseType = getCourseType;
		scope.showTrackModal = showTrackModal;
		scope.showCourseModal = showCourseModal;
		scope.addTrack = addTrack;
		scope.addCourse = addCourse;
		scope.showEditModalTrack = showEditModalTrack;
		scope.showEditModalCourse = showEditModalCourse;
		scope.updateTrack = updateTrack;
		scope.updateCourse = updateCourse;
		scope.deleteTrack = deleteTrack;
		scope.deleteCourse = deleteCourse;
		scope.showCourses = showCourses;
		scope.isTrackSelected = isTrackSelected;
		scope.isCourseSelected = isCourseSelected;
		scope.getContent = getContent;
		scope.showCourseContent = showCourseContent;
		scope.updateContent = updateContent;
		scope.showModalMessage = showModalMessage;
		scope.switchBool = switchBool;

		function init() {
			scope.loadTracks();
			scope.getCourseType();
		};

		function loadTracks() {
			trackService.getAllTrack().then(function (response) {
				scope.tracks = response;
			}, function (error) {
			})
		};

		function getCourseType() {
			trackService.getCourseType().then(function (response) {
				scope.courseTypes = response;
			}, function (error) {
			})
		};

		function showTrackModal(form) {
			scope.modalTrackAlert = false;
			scope.trackName = null;
			scope.isAddTrack = true;
			trackService.resetFormFieldControls(form);
		};

		function showCourseModal(form) {
			scope.modalAlert = false;
			scope.courseName = null;
			scope.isAddCourse = true;
			trackService.resetFormFieldControls(form);
		};

		function addTrack(form) {
			if (!form.$valid) {
				$("#addTrackForm .ng-pristine.ng-invalid").addClass("ng-dirty").removeClass("ng-pristine");
				return false;
			}

			trackService.addTrack(scope.trackName).then(function (response) {
				if (response) {
					scope.showModalMessage('modalTrackAlert', 'trackMessage', "Track added successfully.", alertType[1]);
					scope.loadTracks();
				}
				else {
					scope.showModalMessage('modalTrackAlert', 'trackMessage', "Track could not be added.", alertType[2]);
				}
			}, function (error) {
				scope.showModalMessage('modalTrackAlert', 'trackMessage', "Track could not be added.", alertType[3]);
			})
		};

		function addCourse(form) {
			if (!form.$valid) {
				$("#addCourseForm .ng-pristine.ng-invalid").addClass("ng-dirty").removeClass("ng-pristine");
				return false;
			}

			trackService.addCourse(scope.trackId, scope.courseName).then(function (response) {
				if (response) {
					scope.showModalMessage('modalCourseAlert', 'courseMessage', "Course added successfully.", alertType[1]);
					scope.showCourses(scope.trackId);
				}
				else {
					scope.showModalMessage('modalCourseAlert', 'courseMessage', "Course could not be added.", alertType[2]);
				}
			}, function (error) {
				scope.showModalMessage('modalCourseAlert', 'courseMessage', "Course could not be added.", alertType[3]);
			})
		};

		function showEditModalTrack(trackName, trackId) {
			scope.modalAlert = false;
			scope.isAddTrack = false;
			scope.trackName = trackName;
			scope.trackId = trackId;
		};

		function showEditModalCourse(courseName, courseId, courseType) {
			scope.modalCourseAlert = false;
			scope.isAddCourse = false;
			scope.courseName = courseName;
			scope.courseId = courseId;
			scope.courseType = courseType;
		};

		function updateTrack(form) {
			if (!form.$valid) {
				$("#addTrackForm .ng-pristine.ng-invalid").addClass("ng-dirty").removeClass("ng-pristine");
				return false;
			}

			trackService.updateTrack(scope.trackId, scope.trackName).then(function (response) {
				if (response) {
					scope.showModalMessage('modalTrackAlert', 'trackMessage', "Track updated successfully.", alertType[1]);
					scope.loadTracks();
				}
				else {
					scope.showModalMessage('modalTrackAlert', 'trackMessage', "Track could not be updated.", alertType[2]);
				}
			}, function (error) {
				scope.showModalMessage('modalTrackAlert', 'trackMessage', "Track could not be updated.", alertType[3]);
			})
		};

		function updateCourse(form) {
			if (!form.$valid) {
				$("#addCourseForm .ng-pristine.ng-invalid").addClass("ng-dirty").removeClass("ng-pristine");
				return false;
			}

			trackService.updateCourse(scope.courseId, scope.courseName).then(function (response) {
				if (response) {
					scope.showModalMessage('modalCourseAlert', 'courseMessage', "Course updated successfully.", alertType[1]);
					scope.showCourses(scope.trackId);
				}
				else {
					scope.showModalMessage('modalCourseAlert', 'courseMessage', "Course could not be updated.", alertType[2]);
				}
			}, function (error) {
				scope.showModalMessage('modalCourseAlert', 'courseMessage', "Course could not be added.", alertType[3]);
			})
		};

		function deleteTrack(trackId) {
			trackService.deleteTrack(trackId).then(function (response) {
				if (response) {
					scope.showModalMessage('modalTrackAlert', 'trackMessage', "Track deleted successfully.", alertType[1]);
					scope.loadTracks();
				}
				else {
					scope.showModalMessage('modalTrackAlert', 'trackMessage', "Track could not be deleted.", alertType[2]);
				}
			}, function (error) {
				scope.showModalMessage('modalTrackAlert', 'trackMessage', "Track could not be deleted.", alertType[3]);
			})
		};

		function deleteCourse(courseId) {
			trackService.deleteCourse(courseId).then(function (response) {
				if (response) {
					scope.showModalMessage('modalTrackAlert', 'trackMessage', "Track deleted successfully.", alertType[1]);
					scope.showCourses(scope.trackId);
				}
				else {
					scope.showModalMessage('modalTrackAlert', 'trackMessage', "Track could not be deleted.", alertType[2]);
				}
			}, function (error) {
				scope.showModalMessage('modalTrackAlert', 'trackMessage', "Track could not be deleted.", alertType[3]);
			})
		};

		function showCourses(trackId) {
			scope.trackId = trackId;
			scope.showCourse = true;
			scope.courseSelected = false;
			scope.courseId = null;
			trackService.showCourses(trackId).then(function (response) {
				if (response.length <= 0) {
					scope.CourseEmpty = true;
					scope.courses = null;
					return;
				}
				else {
					scope.CourseEmpty = false;
				}
				scope.courses = response;
			}, function (error) {
			})
		};

		function isTrackSelected(selection) {
			return scope.trackId == selection.trackId;
		}

		function isCourseSelected(selection) {
			return scope.courseId == selection.courseId;
		}

		function getContent(courseId) {
			scope.courseId = courseId;
			scope.courseSelected = true;
			trackService.getContent(courseId).then(function (response) {
				scope.content = response;
				var content = response != "" ? response : emptyContent;
				CKEDITOR.instances.editor.setData(content);
			}, function (error) {
			});
		};

		function showCourseContent(courseId) {
			trackService.getContent(courseId).then(function (response) {
				var content = response != "" ? response : emptyContent;
				trackService.setContent(content);
			}, function (error) {
			});
		};

		function updateContent() {
			var content = CKEDITOR.instances.editor.getData();
			if (content.trim() == emptyContent || content.trim() == "") {
				//scope.showMessage()
				return false;
			}
			trackService.updateContent(scope.courseId, content).then(function (response) {
				CKEDITOR.instances.editor.setData(content);
			}, function (error) {
			});
		}

		//#region to delete
		//function showMessage(modalAlert, message, alertMessageType) {
		//	if (modalAlert === false) {
		//		scope.modalAlert = !scope.modalAlert;
		//	}
		//	scope.alertType = alertMessageType;
		//	switch (alertMessageType) {
		//		case alertType[1]:
		//			scope.alertModalClass = "alert-success";
		//			break;;
		//		case alertType[2]:
		//			scope.alertModalClass = "alert-warning";
		//			break;
		//		case alertType[3]:
		//			scope.alertModalClass = "alert-error";
		//			break;
		//		default:
		//	}
		//	scope.trackMessage = message;
		//}

		//function showCourseMessage(modalAlert, message, alertMessageType) {
		//	if (modalAlert === false) {
		//		scope.modalCourseAlert = !scope.modalCourseAlert;
		//	}
		//	scope.alertType = alertMessageType;
		//	switch (alertMessageType) {
		//		case alertType[1]:
		//			scope.alertCourseModalClass = "alert-success";
		//			break;;
		//		case alertType[2]:
		//			scope.alertCourseModalClass = "alert-warning";
		//			break;
		//		case alertType[3]:
		//			scope.alertCourseModalClass = "alert-error";
		//			break;
		//		default:
		//	}
		//	scope.courseMessage = message;
		//}
		//#endregion

		//show alert message in Modal
		function showModalMessage(modalAlert, alterMessage, message, alertMessageType, timeOut) {
			timeOut = timeOut || 1000;
			if (scope[modalAlert] === false) {
				scope[modalAlert] = !scope[modalAlert];
			}
			//scope.alertType = alertMessageType;
			switch (alertMessageType) {
				case alertType[1]:
					scope.alertModalClass = "alert-success";
					break;;
				case alertType[2]:
					scope.alertModalClass = "alert-warning";
					break;
				case alertType[3]:
					scope.alertModalClass = "alert-error";
					break;
				default:
			}
			scope[alterMessage] = message;
			$timeout(function () {
				scope[modalAlert] = false;
			}, timeOut);
		};
		function switchBool(value) {
			scope[value] = !scope[value];
		};
	}
})();