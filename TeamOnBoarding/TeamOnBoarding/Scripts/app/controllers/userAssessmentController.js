﻿(function () {
	'use strict';

	angular.module("BaseModule").controller("UserAssessmentController", userAssessmentdule);

	function userAssessmentdule($scope, $http, $document, $location, userAssessmentService, alertType) {
		//#region scope variables
		$scope.modalAlert = false;
		$scope.options = [];
		$scope.value = 1;
		$scope.questionTitle = "";
		$scope.questionType = "";
		$scope.courseId = "";
		$scope.saveType = "";
		$scope.assessmentSubmitted = false;
		$scope.userAssessmentStatus = "";
		$scope.assessmentQuestions = [];
		$scope.questionResponses = {};
		$scope.submitted = false;
		//#endregion

		$scope.init = function (QuestionCount, minutes, seconds, userAssessmentStatus) {
			$scope.userAssessmentStatus = userAssessmentStatus;
			if (minutes == 0 && seconds == 0) {
				if (QuestionCount > 0) {
					$scope.QuestionEmpty = false;
				}
				else {
					$scope.QuestionEmpty = true;
				}
				return;
			}
			$scope.minutes = minutes;
			$scope.seconds = seconds;
			if (QuestionCount > 0) {
				$scope.QuestionEmpty = false;
			}
			else {
				$scope.QuestionEmpty = true;
			}
		};

		$scope.showQuestionModal = function () {
			$scope.setUserAssessmentStatus();
		};

		$scope.setUserAssessmentStatus = function () {
			userAssessmentService.setUserAssessmentStatus($scope.courseId).then(function (response) {
				if (!response) {
					return;
				}
				$("#questionModal").addClass("in").css("display", "block");
				if (angular.element('.modal-backdrop').length <= 0) {
					$('<div class="modal-backdrop fade in" />').appendTo($document.prop('body'));
				}
				else {
					angular.element(document.querySelector(".modal-backdrop")).addClass("in").show();
				}
				var windowHeight = $(window).height();
				$(".question-container .modal-body").css("height", windowHeight - 180);
				var modalBodyHeight = $(".question-container .modal-body").height();
				$(".question-container .modal-body .question-track").css("height", modalBodyHeight);
				$scope.getQuestions();
			}, function (error) {
			});
		};

		$scope.closeAssessmentModal = function () {
			$("#questionModal").removeClass("in").fadeOut();
			var modalBackdrop = angular.element(document.querySelector(".modal-backdrop"));
			modalBackdrop.removeClass("in").fadeOut();
		};

		$scope.getQuestions = function () {
			userAssessmentService.getQuestions($scope.courseId).then(function (response) {
				$scope.assessmentQuestions = response;
				$scope.countdown("time-content", $scope.minutes, $scope.seconds);
			}, function (error) {
			})
		}

		$scope.submitTest = function () {
			var response = [];
			$scope.submitted = true;
			$.each($scope.assessmentQuestions, function (index, value) {
				response.push({ questionId: value.QuestionId, questionType: value.QuestionType, response: $scope.questionResponses[value.QuestionId] });
			});
			userAssessmentService.submitTest(response, $scope.courseId).then(function (response) {
				$("#questionModal").removeClass("in").css("display", "none");
				angular.element(document.querySelector(".modal-backdrop")).removeClass("in").hide();
				$scope.assessmentSubmitted = true;
				if (response) {
					$scope.assessmentSubmittedWithErrors = false;
				}
				else {
					$scope.assessmentSubmittedWithErrors = true;
				}
			}, function (error) {
			})
		};

		$scope.countdown = function (elementName, minutes, seconds) {
			var element, endTime, msLeft, hours, mins, time;

			function twoDigits(n) {
				return (n <= 9 ? "0" + n : n);
			}

			function updateTimer() {
				msLeft = endTime - (+new Date);
				if (!$scope.submitted) {
					if (msLeft < 1000) {
						$scope.submitTest();
					} else {
						time = new Date(msLeft);
						hours = time.getUTCHours();
						mins = time.getUTCMinutes();
						element.innerHTML = (hours ? hours + ':<span class="min">' + twoDigits(mins) : mins) + '</span>:' + twoDigits(time.getUTCSeconds());
						//warning before 1 min
						if (msLeft < 61000) {
							//warning before 10 seconds
							if (msLeft < 11000) {
								$(element).css("font-weight", "bold");
							}
							setInterval(blinker, 1000);
						}
						setTimeout(updateTimer, time.getUTCMilliseconds() + 500);
					}
				}
			}

			function blinker() {
				$(element).fadeOut(500);
				$(element).fadeIn(500);
			}

			element = document.getElementById(elementName);
			endTime = (+new Date) + 1000 * (60 * minutes + seconds) + 500;
			updateTimer();
		}

		$scope.scrollToQuestion = function (questionIndex) {
			var questionIndexElement = $(".questions-container #question-" + questionIndex);
			$('.modal-body').animate({
				'scrollTop': Math.abs($(".questions-container").position().top - questionIndexElement.position().top)
			});
		};
	}
})();