﻿(function () {
	'use strict';

	angular.module("BaseModule").controller("MenuController", menuModule);

	menuModule.$inject = ["$location"];

	function menuModule($location) {
		var path = $location.absUrl();
		//path.split('/')[path.split('/').length - 1].split('?').length
		var currentPath = path.split('/')[path.split('/').length - 1];
		var menuPath = currentPath.split('?');
		var isSimplePath = menuPath.length <= 1;
		var menuElement = '';
		if (isSimplePath) {
			menuElement = angular.element(document.querySelector("#" + currentPath));
		}
		else {
			if (menuPath[1].indexOf('courseId') === 0) {
				var courseId = menuPath[1].split('=')[1];
				var menu = menuPath[0];
				menuElement = angular.element(document.querySelector("#"+menu+"Course-" + courseId));
			}
		}
		menuElement.find("a").css("color", "#ffffff");
		menuElement.parents('ul').css("display", "block");
	}
})();