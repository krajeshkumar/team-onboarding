﻿(function () {
	'use strict';

	angular.module("BaseModule")
	.service("constantsService", constantsService);

	constantsService.$inject = ['$http', '$q'];

	function constantsService($http, $q) {
		var roles = [{ key: "user", value: "User" },
					{ key: "admin", value: "Admin" },
					{ key: "evaluator", value: "Evaluator" }];
		this.getRoles =	function () {
			return roles;
		};
	}
})();