﻿(function () {
	'use strict';

	angular.module("BaseModule")
		.factory("trackService", trackFactory);

	trackFactory.$inject = ['$http', '$q', 'baseService'];

	function trackFactory($http, $q, baseService) {
		var content = "";
		var track = Object.create(baseService);
		track.getAllTrack = getAllTrack;
		track.getCourseType = getCourseType;
		track.addCourse = addCourse;
		track.addTrack = addTrack;
		track.updateTrack = updateTrack;
		track.updateCourse = updateCourse;
		track.showCourses = showCourses;
		track.getContent = getContent;
		track.updateContent = updateContent;
		track.setContent = setContent;
		track.deleteTrack = deleteTrack;
		track.deleteCourse = deleteCourse;
		//var track = {
		//	getAllTrack: getAllTrack,
		//	getCourseType: getCourseType,
		//	addCourse: addCourse,
		//	addTrack: addTrack,
		//	updateTrack: updateTrack,
		//	updateCourse: updateCourse,
		//	showCourses: showCourses,
		//	getContent: getContent,
		//	updateContent: updateContent,
		//	setContent: setContent
		//};
		return track;

		function getAllTrack() {
			var deferred = $q.defer();
			$http.get("/Track/GetAllTrack").success(function (data) {
				deferred.resolve(data);
			}).error(function (error) {
				deferred.reject(error);
			});
			return deferred.promise;
		}
		function getCourseType() {
			var deferred = $q.defer();
			$http.get("/Track/GetCourseType").success(function (data) {
				deferred.resolve(data);
			}).error(function (error) {
				deferred.reject(error);
			});
			return deferred.promise;
		}
		function addCourse(trackId, courseName) {
			var deferred = $q.defer();
			var dataSource = { trackId: trackId, courseName: courseName};
			$http.post("/Track/AddCourse", dataSource).success(function (data) {
				deferred.resolve(data);
			}).error(function (error) {
				deferred.reject(error);
			});
			return deferred.promise;
		}
		function addTrack(trackName) {
			var deferred = $q.defer();
			var dataSource = { trackName: trackName };
			$http.post("/Track/AddTrack", dataSource).success(function (data) {
				deferred.resolve(data);
			}).error(function (error) {
				deferred.reject(error);
			});
			return deferred.promise;
		}
		function updateTrack(trackId, trackName) {
			var deferred = $q.defer();
			var dataSource = { id: trackId, trackName: trackName };
			$http.post("/Track/UpdateTrack", dataSource).success(function (data) {
				deferred.resolve(data);
			}).error(function (error) {
				deferred.reject(error);
			});
			return deferred.promise;
		}
		function updateCourse(courseId, courseName) {
			var deferred = $q.defer();
			var dataSource = { id: courseId, courseName: courseName };
			$http.post("/Track/UpdateCourse", dataSource).success(function (data) {
				deferred.resolve(data);
			}).error(function (error) {
				deferred.reject(error);
			});
			return deferred.promise;
		}
		function showCourses(trackId) {
			var deferred = $q.defer();
			$http.get("/Track/GetCourses/?trackId=" + trackId).success(function (data) {
				deferred.resolve(data);
			}).error(function (error) {
				deferred.reject(error);
			});
			return deferred.promise;
		}
		function getContent(courseId) {
			var deferred = $q.defer();
			$http.get("/Track/GetContent/?courseId=" + courseId).success(function (data) {
				deferred.resolve(data);
			}).error(function (error) {
				deferred.reject(error);
			});
			return deferred.promise;
		}
		function updateContent(courseId, content) {
			var deferred = $q.defer();
			var dataSource = { courseId: courseId, content: content };
			$http.post("/Track/UpdateContent", dataSource).success(function (data) {
				deferred.resolve(data);
			}).error(function (error) {
				deferred.reject(error);
			});
			return deferred.promise;
		}
		function setContent(content) {
			this.content = content;
		}
		function deleteTrack(trackId) {
			var deferred = $q.defer();
			var dataSource = { trackId: trackId };
			$http.post("/Track/DeleteTrack", dataSource).success(function (data) {
				deferred.resolve(data);
			}).error(function (error) {
				deferred.reject(error);
			});
			return deferred.promise;
		}
		function deleteCourse(courseId) {
			var deferred = $q.defer();
			var dataSource = { courseId: courseId };
			$http.post("/Track/DeleteCourse", dataSource).success(function (data) {
				deferred.resolve(data);
			}).error(function (error) {
				deferred.reject(error);
			});
			return deferred.promise;
		}
	};
})();