﻿(function () {
	'use strict';

	angular.module("BaseModule")
		.factory("userAssessmentService", userAssessmentFactory);

	userAssessmentFactory.$inject = ['$http', '$q'];
	function userAssessmentFactory($http, $q) {
		var content = "";
		var userAssessment = { getQuestions: getQuestions, setUserAssessmentStatus: setUserAssessmentStatus, submitTest: submitTest };
		return userAssessment;

		function getQuestions(courseId) {
			var deferred = $q.defer();
			$http.get("/UserAssessment/GetQuestions/?courseId=" + courseId).success(function (data) {
				deferred.resolve(data);
			}).error(function (error) {
				deferred.reject(error);
			});
			return deferred.promise;
		};
		function setUserAssessmentStatus(courseId) {
			var deferred = $q.defer();
			$http.post("/UserAssessment/SetUserAssessmentStatus/?courseId=" + courseId).success(function (data) {
				deferred.resolve(data);
			}).error(function (error) {
				deferred.reject(error);
			});
			return deferred.promise;
		};
		function submitTest(response, courseId) {
			var deferred = $q.defer();
			var dataSource = { questionResponse: response, courseId: courseId }
			$http.post("/UserAssessment/SubmitTest", dataSource).success(function (data) {
				deferred.resolve(data);
			}).error(function (error) {
				deferred.reject(error);
			});
			return deferred.promise;
		};
	};
})();