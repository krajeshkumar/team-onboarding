﻿(function () {
	'use strict';

	angular.module("BaseModule")
		.factory("userService", userFactory);

	userFactory.$inject = ['$http', '$q', 'baseService'];
	function userFactory($http, $q, baseService) {
		var firstname = "";
		var lastname = "";
		var email = "";
		var username = "";
		var gender = "";
		var role = "";
		var isactive = "";
		var userid = "";
		var track = "";
		var userDetails = Object.create(baseService);
		userDetails.getUserGridData = getUserGridData;
		userDetails.addUser = addUser;
		userDetails.updateUser = updateUser;
		userDetails.setUserDetails = setUserDetails;
		userDetails.getUserDetails = getUserDetails;
		userDetails.deleteUser = deleteUser;
		return userDetails;

		function getUserGridData(pageSize, page) {
			var deferred = $q.defer();
			var dataToSend = { PageSize: pageSize, CurrentPage: page };
			$http.get("/User/GetUsers/?PageSize=" + pageSize + "&CurrentPage=" + page).success(function (data) {
				deferred.resolve(data);
			}).error(function (error) {
				deferred.reject(error);
			});
			return deferred.promise;
		};
		function addUser(firstName, lastName, email, userName, gender, role, password, track) {
			var deferred = $q.defer();
			var dataSource = { firstName: firstName, lastName: lastName, email: email, userName: userName, gender: gender, role: role, password: password, trackId: track };
			$http.post("/User/AddUser", dataSource).success(function (data) {
				deferred.resolve(data);
			}).error(function (error) {
				deferred.reject(error);
			});
			return deferred.promise;
		};
		function updateUser(userId, firstName, lastName, email, userName, gender, track) {
			var deferred = $q.defer();
			var dataSource = { id: userId, firstName: firstName, lastName: lastName, email: email, userName: userName, gender: gender, trackId: track };
			$http.post("/User/UpdateUser", dataSource).success(function (data) {
				deferred.resolve(data);
			}).error(function (error) {
				deferred.reject(error);
			});
			return deferred.promise;
		};
		function setUserDetails(userid, firstname, lastname, username, email, role, isactive, gender, trackId) {
			this.userid = userid;
			this.firstname = firstname;
			this.lastname = lastname;
			this.email = email;
			this.username = username;
			this.gender = gender;
			this.role = role;
			this.isactive = isactive;
			this.trackId = trackId;
		};
		function getUserDetails() {
			return {
				userid: this.userid,
				firstname: this.firstname,
				lastname: this.lastname,
				email: this.email,
				username: this.username,
				gender: this.gender,
				role: this.role,
				trackId: this.trackId,
				isactive: this.isactive
			}
		};
		function deleteUser(userId) {
			var deferred = $q.defer();
			var dataSource = { userId: userId };
			$http.post("/User/DeleteUser", dataSource).success(function (data) {
				deferred.resolve(data);
			}).error(function (error) {
				deferred.reject(error);
			});
			return deferred.promise;
		};
	};
})();