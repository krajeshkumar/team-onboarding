﻿(function () {
	'use strict';

	angular.module("BaseModule")
		.factory("assessmentService", assessmentService);

	assessmentService.$inject = ['$http', '$q', 'baseService'];

	function assessmentService($http, $q, baseService) {
		var content = "";
		var questionFactory = Object.create(baseService);
		questionFactory.saveQuestion = saveQuestion;
		questionFactory.deleteQuestion = deleteQuestion;
		questionFactory.getQuestions = getQuestions;
		return questionFactory;

		function saveQuestion(model, saveType) {
			var deferred = $q.defer();
			if (saveType == "add") {
				$http.post("/Assessment/SaveQuestion", model).success(function (data) {
					deferred.resolve(data);
				}).error(function (error) {
					deferred.reject(error);
				});
			}
			if (saveType == "update") {
				$http.post("/Assessment/UpdateQuestion", model).success(function (data) {
					deferred.resolve(data);
				}).error(function (error) {
					deferred.reject(error);
				});
			}
			return deferred.promise;
		};
		function deleteQuestion(questionId) {
			var deferred = $q.defer();
			$http.get("/Assessment/DeleteQuestion/?questionId=" + questionId).success(function (data) {
				deferred.resolve(data);
			}).error(function (error) {
				deferred.reject(error);
			});
			return deferred.promise;
		};
		function getQuestions(courseId) {
			var deferred = $q.defer();
			$http.get("/Assessment/GetQuestions/?courseId=" + courseId).success(function (data) {
				deferred.resolve(data);
			}).error(function (error) {
				deferred.reject(error);
			});
			return deferred.promise;
		};
	};
})();