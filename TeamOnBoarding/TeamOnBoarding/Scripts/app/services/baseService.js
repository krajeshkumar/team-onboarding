﻿(function () {
	'use strict';

	angular.module("BaseModule")
	.factory("baseService", baseService);

	baseService.$inject = ['$http', '$q'];

	function baseService($http, $q) {
		var content = "";
		var courseName = "";
		var contentFactory = {
			getContent: getContent,
			setContent: setContent,
			getCourseContent: getCourseContent,
			resetFormFieldControls: resetFormFieldControls
		};
		return contentFactory;

		function getContent(courseId) {
			var deferred = $q.defer();
			$http.get("/Home/GetContent/?courseId=" + courseId).success(function (data) {
				deferred.resolve(data);
			}).error(function (error) {
				deferred.reject(error);
			});
			return deferred.promise;
		};
		function setContent(content, courseName) {
			this.content = content;
			this.courseName = courseName;
		};
		function getCourseContent() {
			return { content: this.content, courseName: this.courseName };
		};
		//Reset form controls
		function resetFormFieldControls(form) {
			if (form) {
				form.$setPristine();
				form.$setUntouched();
			}
		};
	}
})();