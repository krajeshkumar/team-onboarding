﻿(function () {
	'use strict';

	angular.module("BaseModule")
	.factory("loginService", loginService);

	loginService.$inject = ['$http', '$q'];
	function loginService($http, $q) {
		var loginFactory = { getLogin: getLogin };
		return loginFactory;

		function getLogin(username, password) {
			var deferred = $q.defer();
			$http.get("/Login/GetUserLogin/?username=" + username + "&password=" + password).success(function (data) {
				deferred.resolve(data);
			}).error(function (error) {
				deferred.reject(error);
			});
			return deferred.promise;
		};
	}
})();