﻿(function () {
	angular.module("BaseModule",
		["ngRoute",
			"ngGrid",
			"ngMessages",
			"ngSanitize"
		]);

	//angular.module("BaseModule")
	//.config(["$routeProvider", "$locationProvider", function ($routeProvider, $locationProvider) {
	//	$locationProvider.html5Mode(true);
	//}])
	angular.module("BaseModule")
		.config(["$httpProvider", function ($httpProvider) {
			$httpProvider.defaults.useXDomain = true;
			delete $httpProvider.defaults.headers.common["X-Requested-With"];
		}])
		.constant("alertType", { 1: "success", 2: "warning", 3: "error" });
})();